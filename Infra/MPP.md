# MPP DB 정리
## 종류
-------------------------
![MPP DB 종류](https://images.ctfassets.net/vrkkgjbn4fsk/5UGUPAao6cCCKwW2CUc6qS/fa91f22cf4eed567f9f0912b352d4775/data-warehousing-options-table.png?q=90&w=1460)

## Amazon Redshift
------------------------------
### Architecture
![Redshift architecture](https://docs.aws.amazon.com/ko_kr/redshift/latest/dg/images/02-NodeRelationships.png)
- 리더 노드 
    - SQL 연결 엔드 포인트
    - 메타 데이터 저장
    - 클러스터의 모든 쿼리 수행을 관리
- 컴퓨팅 노드
    - 로컬, 컬럼 방식 스토리지
    - 병렬로 쿼리 수행
    - s3 기반으로 로딩, 백업, 복구 수행
    - DynamoDB, EC2 호스트로 부터 병렬 로딩
- 슬라이스 노드
    - 개별 노드는 복수의 슬라이스로 구성
    - 슬라이스는 별도의 메모리, CPU 및 Disk 공간 할당
    - 슬라이스 별로 독립적인 워크로드를 병렬로 실행
    
### 특징
- Colmnar 스토리지, 데이터 압축   
- PostgreSQL 기반으로 SQL 지원
- Auto Scaling을 통해 규모를 탄력적으로 운용 가능
- CLI를 제공, 다양한 API 지원
- 데이터 웨어하우스 워크플로우 예제
![](img/dw_workflow_example.PNG)

### 장점
- 전통적인 db보다 100배 빠름, GB부터 PB가지 확장 가능, 스토리지 제약 없이 데이터 분석 가능
- MPP DB보다 10배 저렴, 운영하고 제공(provision)하기 쉽다, 높은 생산성
- Hadoop보다 10배 빠름, 프로그래밍 필요없음, standard interfaces and integration tp ;everage BI tools, machine learning, streaming

![migration from greenplum @NTT Docomo](https://image.slidesharecdn.com/amazonredshiftmigrationbestpractices-generic-170223203619/95/best-practices-for-migrating-your-data-warehouse-to-amazon-redshift-10-1024.jpg?cb=1487882248)

## Greenplum
---------------------------------------------------
### Architecture
![Greenplum architecture](https://gpdb.docs.pivotal.io/560/admin_guide/graphics/highlevel_arch.jpg)
- master
    - 클라이언트 연결 및 SQL 쿼리를 수락하고 작업을 세그먼트에 분배
    - 시스템에 대한 메타데이터 저장
- segment
    - 데이터를 저장하고 쿼리 처리를 수행
    - 데이터는 각 세그먼트에 병렬로 로드

### 특징
- 오픈 소스
- 주요 클라우드(AWS, GCP, Azure) 마켓플레이스에서 컨테이너 이용하여 배포 가능
- PostgreSQL 기반으로 SQL을 지원
- pxf를 통해 여러 데이터 소스에서 데이터를 가지고 올 수 있음
- Kafka, redis 등과 하여 실시간 데이터를 수집/처리 
![](https://t1.daumcdn.net/cfile/tistory/9980BE475B6EC8E10C)





## 참고
- [AWS 기반 데이터 웨어하우징](https://d1.awsstatic.com/whitepapers/ko_KR/enterprise-data-warehousing-on-aws.pdf)
- [pivotla docs - greenplum 6.2](https://gpdb.docs.pivotal.io/6-2/main/index.html)
- [GPDB 개념 블로그](https://stricky.tistory.com/55?category=1013508)
- [redshift, greenplum, postgresql comparison](https://db-engines.com/en/system/Amazon+Redshift%3BGreenplum%3BPostgreSQL)
- [AWS 기능 및 장점 정리](https://postitforhooney.tistory.com/entry/%EC%9E%90%EC%A3%BC-%EC%82%AC%EC%9A%A9%EB%90%98%EB%8A%94-AWS-%EA%B8%B0%EB%8A%A5-%EB%B0%8F-%EC%9E%A5%EC%A0%90-%EC%A0%95%EB%A6%AC)
- [AWS redshift 개요](https://aws.amazon.com/ko/redshift/?c=db&sec=srv)
- [Amazon Redshift의 이해와 활용](https://www.slideshare.net/awskorea/amazon-redshift-deep-dive)
- [Greenplum Poadmap slide](https://www.slideshare.net/PivotalKorea/greenplum-roadmap)
- [redshift vs greenplum comparison](https://www.itcentralstation.com/products/comparisons/amazon-redshift_vs_pivotal-greenplum)
- [google group GPDB vs redshift](https://groups.google.com/a/greenplum.org/forum/#!searchin/gpdb-users/redshift%7Csort:date/gpdb-users/7uQTZ-verAw/HO6CVc_0DQAJ)
- [Greenplum versus redshift and actian vectorwise comparison](https://www.slideshare.net/syedhamin/greenplum-versus-redshift-and-actian-vectorwise-comparison-79582099)
- [choosing between modern data warehouses](https://dzone.com/articles/choosing-between-modern-data-warehouses)

- [best practices for migrating your data warehouse to amazon redshift](https://www.slideshare.net/AmazonWebServices/best-practices-for-migrating-your-data-warehouse-to-amazon-redshift)
- [how redshift differs from postgresql](https://www.stitchdata.com/blog/how-redshift-differs-from-postgresql/)