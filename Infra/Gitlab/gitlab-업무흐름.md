# gitlab 프로젝트 업무 흐름
___________________________
![Gitlab 프로젝트 구성](http://developer.gaeasoft.co.kr/development-guide/workflow/img/gitlab-group.png)
## 프로젝트 구성
- 네임스페이스
- 프로젝트
- 마일스톤
- 이슈 
- member : 
![Gitlab 프로젝트 업무흐름(workflow)](http://developer.gaeasoft.co.kr/development-guide/workflow/img/gitlab-workflow-main.png)

## 업무 흐름 단계
1. group 생성
2. member 추가
3. 프로젝트 생성
4. 마일스톤 등록
5. 이슈 발행
6. 이슈 종료

  
# 참조
- [gitlab 기반시스템을 이용한 workflow 가이드](http://developer.gaeasoft.co.kr/development-guide/workflow/gitlab-workflow-guide/)