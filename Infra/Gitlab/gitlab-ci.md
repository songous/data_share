# gitlab-ci

## CI/CD란?
![CI/CD](https://cdn-images-1.medium.com/max/1600/1*TNJ7Rpr5G1OJHtKH-IBEFw.png)
- CI/CD는 Continuous Integration / Continuous Delivery(Deployment)
- Continuous Integration(CI)
  - 개발자가 각각 개발한 소스코드를 모아서 한꺼번에 빌드하는 통합 빌드의 과정을 특정 시점이 아니라 추가로 수행함으로써 통합에 발생하는 오류를 사전에 해결하고 이러한 과정들에 드는 시간을 줄이기 위한 기법
  - 개발자를 위한 자동화 프로세스
  - 정기적으로 코드의 변경사항을 병합하여 개발자간의 코드 충돌을 방지하기 위한 목적
- Continuous Delivery or Continuous Deployment (CD)
  - 애플리케이션에 적용한 변경사항이 버그 테스트를 거쳐 리포지터리에 자동으로 업로드 되는 것
  - 운영팀은 언제든 실시간으로 이 리포지터리에서 실시간으로 프로덕션 환경으로 배포 가능
 
## 아키텍처
![](https://www.popit.kr/wp-content/uploads/2019/12/Gitlab_Pineplines.png)
![](https://swalloow.github.io/assets/images/gitlab-ci.png)

## 설명 
- gitlab-ci는 gitlab에서 제공하는 CI 툴
- 단계 및 작업은 gitlab-ci.yml를 통해 구성

## 장점
- 우수한 Docker와의 통합
- 같은 단계 내에서의 병렬 작업 실행
- 작업을 쉽게 추가(gitlab-ci.yml에 작성하면 바로 추가 가능)
- 동시 러너로 인한 뛰어난 확장성

## 단점
- 모든 작업은 아티팩트를 정의하고 업로드 및 다운로드가 필요
- 실제로 merge되기 전에는 merge 상태를 테스트할 수 없다.
- 스테이지 내 스테이지는 아직 지원하지 않는다.

## gitlab-ci 사용법
- gitlab runner를 배포가 실행되는 서버에 설치
  - 만약 배포를 클라우드 서비스에 해야 한다면 따로 인터넷이 연결된 곳에 설치해도 상관 없음
- gitlab project에 gitlab runner 등록
- CI/CD의 작업을 정의한 .gitlab-ci.yml파일

## gitlab runner
- .gitlab-ci를 통해 정의한 작업을 실행하는 주체

## .gitlab-ci.yml
- CI/CD 작업을 정의한 yaml 파일
- 파일 작성 예시
```yaml
image: gitlab-registry
stages:
  - build
  - test
  - deploy

job-build:
  stage: build
  script:
  - pip install -r requirements.txt
  - python -m py_compile run.py

job-test:
  stage: test
  script:
  - pytest --pep8 -m pep8 backend/

job-deploy:
  stage: deploy
  script:
  - deployment
```

### [.gitlab-ci.yml 구성 매개 변수](https://docs.gitlab.com/ee/ci/yaml/README.html)
- script : Runner가 실행하는 명령 세트
- image : 도커이미지 사용(image:name, image:entrypoint 사용 가능)
- services : 도커 서비스 이미지 사용(services:name,services:alias,services:entrypoint, services:command 사용 가능)
- before_script, after_script : script 전,후 실행되는 명령 세트
- stages : pipeline의 각 단계를 정의
- stage : 작업이 pipeline의 어느 단계인지 정의
- only : 작업이 생성될 때 제한(only:refs, only:master)
- excepts : 작업이 생성되지 않을때 제한(excepts:refs) 
- tags : 사용할 Runner를 선택하는데 사용하는 태그 목록
- allow_failure : 작업실패를 허용(실패한 작업은 커밋 상태에 기여하지 않음)
- when : 작업 종료 시간(when:manual, when:delayed 사용 가능)
- enviroment : 작업이 배포되는 환경 이름(enviroment:name,enviroment:url)
- cache : 후속 실행사잉에 캐시되어야 하는 파일 목록(cache:paths, cache:key)
- artifacts : 성공시 작업에 첨부할 파일 및 디렉토리 목록 ( artifacts:paths, artifacts:name, artifaces:untracked)
- dependencise: 작업이 의존하는 다른 작업들로서 작업간에 이슈를 전달할 수 있음
- coverage : 주어진 작업에 대한 코드 커버리지 설정
- retry : 작업이 실패한 경우 자동 재시도 할 수 있는 시기와 횟수
- parallel : 얼마나 많은 작업 인스턴스가 병렬로 실행되어야 하는지
- trigger : 다운 스트림 pipeline 트리거 정의
- include : 작업이 외부 YAML 파일을 포함할 수 있게 함
   - include:local, include:file, include: template, include:remote
- extends: 이 작업이 상속할 구성 목록
- pages : gilav pages에서 사용할 작업의 결과를 업로드
- variables : 작업 레벨에서 작업 변수를 정의