# gitlab-runner on linux
-------------------------------
## 개요
- ubuntu에서 docker에 

## 환경
- os : ubuntu 18.03
- docker
  - server : 18.09.7
  - client : 19.03.5

  
## git runner 설치
- [gitlab_runner(window) 참고](gitlab-runner(window).md/#Gitlab-runner-with-docker)

## DooD를 사용하여 컨테이너 내부에서 외부 도커 사용하기
- gitlab container를 생성할 때 `-v /var/run/docker.sock:/var/run/docker.sock`를 사용
- gitlab-runner의 `config.toml`에서 <br>
`volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]`를 추가

![](img/runner_executing.PNG)