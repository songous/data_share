# gitlab 특징
-----------------------------------
## 장점
- repo 당 10GB까지 사용 가능, 무제한 private repo(2015)
- git lfs 사용 가능 (github는 1GB/month 무료)

## 단점
- gitlab에서 페이지간의 이동이 느림
- 서버가 불안정

## 문제
- 고정 ip 필요 (server에 직접 설치시)
- group issue board에서 다중 board 지원은 유료 버전

## 참고
- 소스 트리([source tree](https://steps4great.tistory.com/15))
- [redmine 이슈 트랙커](https://docs.gitlab.com/ee/user/project/integrations/redmine.html)


# docker를 이용한 gitlab 설치  
-----------------------------
- 환경 : windows10
- docker version : 19.03.5

## [설치](https://devyurim.github.io/development%20environment/docker/2018/06/27/docker-1.html)
- gitlab 데이터를 저장할 디렉토리를 만든다
- ```docker run --detach --hostname <hostname> --publish port:80 --publish <port_num>:22 --name gitlab --restart always --volume --volume /c/gitlab/config:/home/ailab/GitLab --volume /c/gitlab/logs:/home/ailab/GitLab/log --volume /c/gitlab/data:/home/ailab/GitLab/data gitlab/gitlab-ce:latest```
    - 포트번호 80은 http, 22 ssh deamon용 포트. 도커 포트 맵핑은 호스트포트:컨테이너 포트로 이루어짐
    - `EORROR : includes invalid characters for a local volume name`: 윈도우 경로도 C 앞에 /를 붙여야함. 리눅스식 경로로 작성할 것

## configuration 수정
- `docker exec -it gitlab /bin/bash
cd /etc/gitlab
vi gitlab.rb`
- 설정 파일 gitlab.rb 수정
> external_url 'http://gitlab.ailab.com:8897'
>
>gitlab_rails['gitlab_shell_ssh_port'] = 6015
- 위 설정 파일은 처음 run에서 설정한 config volume에서도 수정 가능


## gitlab-runner
- gitlab에서 CI/CD를 할 수 있는 tool
- shell, docker, k8s 등 여러 환경에서 test,build,deploy 할 수 있다.

## 외부에서 gitlab container registry 접근
- `docker login registry.gitlab.com`
  - userid : gitlab의 user id 입력 
  - psswd : access token 입력
- access token 만들기
  - gitlab 사이트 우측 상단의 icon > setting
  - 왼쪽 배너의 Access Tokens 클릭
  - 원하는 name 입력
  - Scopes로 권한 설정
    - [자세한 권한 확인](https://gitlab.com/help/user/profile/personal_access_tokens)

- gitlab-runner에서 docker login하기
  - user id와 passwd를 변수에 등록하기
    - project > setting > CI/CD > variables


