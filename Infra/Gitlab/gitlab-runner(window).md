# gitlab-runner on windows
-------------------------------
## 환경
- os: windows 10
- docker : 19.03.5

## localhost(server)에 있는 gitlab runner 사용법
------------------------------------------------------------
- [예제 project](https://gitlab.com/songous/local-gitlab-ci/)
### gitlab-runner 설치
1. [gitlab runner 설치](https://docs.gitlab.com/runner)
    - [gitlab runner docs installation ](https://docs.gitlab.com/runner/install/windows.html) 접속 후 바이너리 파일 다운로드
    - C드라이브에 gitlab-runner 폴더 생성 후 바이너리 파일을 옮기기
    - `cd C:\gitlab-runner
    gitlab-runner.exe install
    gitlab-runner.exe --version`
    - install 이후 바로 사용 가능

### gitlab runner 사용법
    
1. Register : gitlab project에서 사용할 수 있도록 등록 
  - process to bind runner with gitlab instance
  - gitlab project의 setting > CI/CD > Runners Expand
    - Set up a specific Runner manually 및에 URL과 token을 복사
    - runner register의 입력 값에 URL, token을 입력
    - excutor : shell, docker, kubernetes 등이 있음. shell로 입력
    - tag: .gitlab-ci.yaml에서 사용할 별칭. tag를 정확하게 설정하지 않으면 등록한 runner가 실행되지 않음
  - 설치 후 실행한 `gitlab-runner.exe stop`
  - [gitlab register](https://docs.gitlab.com/runner/register/#windows)
  
1. Unregister : 등록된 정보 삭제
  - `gitlab-runner.exe unregister --url <url> --token <token>`
  - url과 token은 config.toml에서 확인 가능
  - 실행이 되지 않을 때는 관리자 권한으로 실행했는지 확인

1. gitlab runner에서 사용안하는 runner 지우기
    - `gitlab-runner.exe verify --delete`
    - `gitlab-runner.exe unregister --name <runner-name>`

1. 참고 : runner 실행 파일을 환경변수에 등록하면 명령어로 사용가능
### windows 환경에서 runner 사용시 장점
- 사용하기 편리함 

### windows 환경에서 runner 사용시 단점
- window의 powershell을 사용하기 때문에 linux 환경(os,container)에서 config 파일을 바로 사용하기 어려움

## Gitlab runner with docker
------------------------------------------------

### 개요
- gitlab runner 설치
- gitlab runner를 이용하여 jupyterhub container 배포
- docker는 설치 되어 있다고 가정
- [window에서 docker gitlab runner 사용법](https://jeremyberglund.com/docker-gitlab-runner/)

### gitlab runner on docker 설치
- `docker pull gitlab/gitlab-runner:latest` : runner 이미지 pulling
- gitlab runner container 실행<br>
  - `docker run -d --name gitlab-runner --restart always -v volume1/docker/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock -v /volume2/backup/gitlab-runner:/backup gitlab/gitlab-runner:latest`
  - `/srv/gitlab-runner/config` : 디렉토리에 register용 key,value가 들어있음
  - windows에서 볼륨은 `/c/path/config` 형식으로 바꾸어 설정
- `docker exec -it gitlab-runner /bin/bash` : container에 접근

## runner 사용법
- 기본적인 사항은 위 방법과 동일하나 등록하기 위해서 container 내부에서 명령어를 사용하거나 config.toml을 공유
- 내부에서 직접 등록하기
  - `docker exec -it <runner container 이름>  /bin/bash`
  - `gitlab-runner --version`으로 설치되었는지 확인
  - `gitlab-runner register` 등 필요한 작업 후 `exit`
- config.toml를 외부에서 관리하기 위해서는 docker container에 volume을 연결하여 외부에서 직접 관리
  
## gitlab-runner를 이용한 jupyter hub CI/CD 구축

### Dockerfile 작성
- docker image를 빌드하기 위한 설정 파일

### jupyterhub_config.py 작성
- jupyterhub의 설정을 위한 py 파일

### .gitlab-ci.yml
- gitlab-runner를 위한 설정 파일은 yaml 형식으로 작성

- gitlab ci pipeline 이미지
![gitlab ci pipeline](https://docs.gitlab.com/ee/ci/img/pipelines.png)
  - 위 pipeline에서 작업 단계는 stage라고 함
  - stage 내의 작업을 job이라고 함. job은 unique한 이름을 가지고 있음
  - 각 작업은 .gitlab-ci.yml에서 정의 
- gitlab runner에 정의된 변수
  - $CI_REGISTRY_IMAGE : 현재 프로젝트의 image registry 경로
    - registry
- 문제 gitlab http://docker:2375/v1.40/version: dial tcp: lookup docker on 192.168.65.1:53: no such host
  - runner 설정에서 --docker-privileged 를 추가 하면 가능
  - 이미 설정이 되어있는 runner는 config.toml에서 privileged: true로 변경
  
- gitlab runner excutor 선택 기준
![](./img/runner_excutor.PNG)



## 참고
- [Gitlab-runner를 이용하여 CI/CD 구축](https://github.com/luckymagic7/Base/wiki/GitLab-Runner%EB%A5%BC-%EC%9D%B4%EC%9A%A9%ED%95%9C-CI-CD)
- [Speed up gitlab-ci using kubernetes and Docker](https://webcloudpower.com/deploy-phoenix-elixir-on-a-kubernetes-cluster-using-helm-2/)


- 