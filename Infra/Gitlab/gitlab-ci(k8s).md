# gitlab-ci & k8s

## gitlab ci
  - kubernetes cluster Auto DevOps 없이도 사용 가능
  - cluster 추가 후 application 추가 
  - helm을 이용한 deployment
  - 카나리아 배포
  - k8s executor
    - 기존의 k8s 클러스터를 사용할 수 있음
    - executor는 k8s 클러스터 api를 호출하고 gitlab ci 작업에 대해 새 빌드를 생성
    - runner로 실행한 executor는 바로 k8s에 연동 가능?
    - 연동시에 어디를 연결해야 하는지(api 서버?)
  - k8s와 Auto DevOps
  
## 참고 자료
1. youtube 영상
  - Commit Brooklyn 2019: Creating a CI/CD Pipeline with GitLab and Kubernetes in 20 Minutes(https://www.youtube.com/watch?v=-shvwiBwFVI)
    - Digital Ocean의 k8s cluster와 연동
    - [동일 내용 gitlb 블로그 post](https://about.gitlab.com/blog/2019/09/26/building-a-cicd-pipeline-in-20-mins/)
    
2. Golang build : kubernetes와 gitlab 연동에 대한 참고 자료
  - [CI/CD on Kubernets with GitLab](https://blog.kubernauts.io/ci-cd-on-kubernetes-with-gitlab-46d2a92b3565)
    - Kubernetes with gitlab cluster 추가시 제공해야할 사항에 대한 자세한 설명 
  - [GitLab + Kubernetes: Using GitLab CI's Kubernetes Cluster feature](https://edenmal.moe/post/2018/GitLab-Kubernetes-Using-GitLab-CIs-Kubernetes-Cluster-feature/)
    - tutorial 같이 step by step으로 설명
    - 간단한.. troubleshooting도 나와 있음
  - [gitlab ci/cd pippelines for k8s cluster](https://icicimov.github.io/blog/devops/GitLab-CICD-Pipelines-for-Kubernetes-clusters/)
    - [github](https://github.com/icicimov/go-app)에 소스코드 공유
    - .gitlab-ci.yml을 확인 가능(다른 곳에서 선언한 job도 사용)
    - k8s보다는 gitlab-ci 설정에 대한 설명이 도움이 됨
  - [gitlab-cicd with golang](https://imti.co/gitlabci-golang-microservices/)
    - 세부 사항에 대해 자세히 설명
    
3. gitlab runner on k8s
  - [Lessons learned with GitLab Runner on Kubernetes](https://medium.com/90seconds/lessons-learned-with-gitlab-runner-on-kubernetes-d547c30ad5fb)
    - GKE를 이용한 연동
    - .gitlab-ci.yml에 대한 라인별 주석도 있음
    - GKE 연동 방법에 대해 step 별로 정리
    - runner가 필요 없음
    -![구조 이미지](https://miro.medium.com/max/4440/1*pjEn0971NoOYdcE3yr-TiA.jpeg)
  - [ibm의 gitlab runner on k8s](https://developer.ibm.com/tutorials/use-gitlab-runners-on-kubernetes/)
    - helm을 사용한 runner 설치
  - [gitlab ci에서 docker build 하는법](http://vlabs.iitb.ac.in/gitlab/help/ci/docker/using_docker_build.md)
4. K8s canary deployment
  - [카나리아 배포 #1](https://levelup.gitconnected.com/kubernetes-canary-deployment-1-gitlab-ci-518f9fdaa7ed)
  
5. helm 이용한 배포
  - [k8s+helm+gitlab-ci-cd](https://sudonull.com/post/11523-Kubernetes-k8s-Helm-GitLab-CI-CD-Properly-deployed)
4. 기타
  - [DinD를 사용한 gitlab-ci에서 k8s로 CD](https://engineering.facile.it/blog/eng/continuous-deployment-from-gitlab-ci-to-k8s-using-docker-in-docker/)
    - config에 대한 자세한 설명이 나와있음
    - docker-compose를 사용하여 이미지 생성
  - [how to improve enhance you k8s ci-cd pipeline](https://thenewstack.io/part-1-how-to-improve-enhance-your-kubernetes-ci-cd-pipelines-with-gitlab-and-open-source/) 
  - [minikube and k8s with gitlab ci](https://blog.ramjee.uk/minikube-and-gitlab-ci/)
    - minikube와 gitlab 연동에 대한 설정에 대해 참고할 것이 많을 것 같음
    - .gitlab-ci.yml deploy에서 참고할 부분이 많음
  - **[existing cluster 추가](https://gitlab.com/help/user/project/clusters/add_remove_clusters.md#add-existing-cluster)**
    - cluster 추가 방법에 대해 자세히 명령어와 같이 나와 있음
    - cluster 추가할 때 보기 좋은 예제
    - gitlab Docs 
  - [kubernetes context 변경](https://bryan.wiki/292)
    - ./kube/config에 대한 간단한 설명
    - kubeconfig 변경 방법
    - 클러스터 2개의 config 병합 방법
  - [jupyter hub runbook with gitalb and k8s](https://gitlab.com/help/user/project/clusters/runbooks/index.md#executable-runbooks)
    - gitlab에 추가된 k8s 클러스터에 jupyterhub 추가하기
    - Nurch? - 나중에 확인
  
    
## gitlab-ci with kubernetes

### k8s 추가 진행 순서
1. DO에 kubernetest cluster 생성
2. kubernetes cluster를 로컬 kubectl에 연결
3. gitlab에 cluster를 추가하기 위한 사전작업
4. cluster 추가

### 1. Digital Ocean에 kubernetes cluster 생성
### 2. kubernetes cluster를 로컬 kubeclt에 연결
1. DO에서 kubeconfig.yaml을 다운로드
2. local에 있는 kubectl에 context 추가하기
  - .kube/config 파일 확인
  - Download한 kubeconfig.yaml 파일에서 cluster, context, name을 복사하여 .kube/config 파일에 붙여넣기
  - contexts 밑에 있는 current-contxt는 하나만 있어야 함. do에서 받은 kubeconfig.yaml에 있는 currnet-context를 입력시 별도로 context를 변경 하지 않아도 바로 사용 가능
  - 아래 명령어를 통해 context 변경 가능
    -`kubectl config use-context <context-name>`
3. 연결 상태 확인
  - `kubectl cluster-info` : 연결된 kubernetes master 확인 가능
  - `kbuectl get nodes` : 현재 연결되어 있는 node 확인 가능
  - 연결이 되어 있지 않다면 config 파일을 제대로 작성했는지와 context를 변경하였는지 확인하기

### 3. gitlab에 cluster를 추가하기 위한 환경 설정
1. 배포시에 사용할 namespace 만들기
  - yaml을 사용하는 방법과 명령어를 사용하는 방법. 2가지가 있음
  - 명령어 사용방법
    - `kubectl create namespace namespace_name`
  - yaml 사용 방법
    - namespace.yaml 파일 작성
    - `kubectl create -f namespace.yaml`
``` yaml
apiVersion: v1
kind: Namespace
metadata:
  name: jupyter
```
    
2. **cluster-admin** 권한이 있는 serviceaccount 만들기
  - gitlab-admin-service-account.yaml 파일 생성
    - [링크](https://gitlab.com/help/user/project/clusters/add_remove_clusters.md#add-existing-cluster)에 있는 file 복사

### 4. cluster 추가 하기
- [참고 사이트](https://blog.zedroot.org/2019/02/12/use-your-do-kubernetes-cluster-with-gitlab-ci/)
- ![](https://blog.zedroot.org/content/images/2019/02/gitlab-kubernetes-integration-form.png)
- __kubernetes cluster name__ : 자신이 원하는 이름(do k8s cluster name으로)
- __API URL__ : do kubernetes cluster config.yaml 파일에서 clusters.clusetr.server에 주소 입력 
- __CA Certificate__
  -do kubernetes cluster config.yaml 파일에서 clusters.clusetr.certificate-authority-data 값 복사
  - `echo <certificate data> | base64 --decode`
  - `-----BEGIN CERTIFICATE-----` 부터 `-----END CERTIFICATE-----`까지 모두 입력
-  __token__
  - `kubectl get sa`을 통해 가지고 와야할 SecretAccount
  -  `kubectl get secret $(kubectl get sa <sa name> --output jsonpath='{.secrets[0].name}') --output jsonpath='{.data.token}' | base64 --decode`
  - `kubectl get secret <scret name> -o jsonpath="{['data']['ca\.crt']}" | base64 --decode`
  - `kubectl get sa <sa name> --output jsonpath='{.secrets[0].name}'` : kubectl get secret의 첫번째 token
  - 출력 결과를 gitlab에 입력
- __Project namespace__ : 빈칸으로 

### CI/CD
- gitlab runner 없이 ci/cd를 사용
  - 공유 gitlab runner가 사용됨
- gitlab runner를 kubernetes에 설치하여 사용
  - install 가능한 application에 gitlab runner가 있음
  - kubeconfig를 포함하여 프로젝트 구성?
  - apllication의 gitalb-runner 설치시 등록까지 자동으로 이루어짐
  - kaniko를 이용하여 docker 없이도 image build가 가능함

## cluter 추가 시 사용 가능한 기능
- auto devops의 deploy
- cluster에 helm을 설치하여 다양한 tool을 자동으로 설치할 수 있음

### k8s cluster로 deployment
- Auto DevOps를 사용
  - 아직 사용 못함
  - auto DevPos 사용시 공용 runner 사용
- gitlab-runner 사용
  - manifast file을 이용하여 배포 가능
  - helm을 사용한 방법을 고민
  - manifast file이나 helm chart를 통해 지속적인 배포를 할 수 있는 script는 고민 중
  - gitlab에서 environment를 설정하면 새로운 namespace를 k8s 클러스터에 생성하여 추가
    - 원하는 namespace를 사용하고 싶을 때 어떻게 하는지 확인 해야함
    - context 변경하는 방법
    - `kubectl scale --replicas=0pps/jupyter-notebook --replicas=0 deployment deployment.apps/jupyter-notebookkubectl scale --replicas=0pps/jupyter-notebook --replicas=0 deployment deployment.apps/jupyter-notebook` : kubernetes pod scale down 하는 법
  - k8s runner에서 build
    - docker:dind service를 사용해 빌드
    - docker:dind의 최신버젼에서는 TLS 인증을 필수적으로 해야한다. 따라서 TLS 인증을 따로 하지 않기 위해서는 docker:18.09.8-dinid 서비스를 사용하면 됨 