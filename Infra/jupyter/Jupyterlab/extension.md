# JupyterLab Extension 

## 설치된 extension list
- Version Control
  - jupyterlab-git : jupyterlab을 위한 git 확장
- formating
  - jupyterlab_code_formatter
- viwer / interaction
  - ipywidgets : jupyter notebook을 위한 상호작용 위젯
  - Jupyterlab-toc : markdown 언어로 작성된 파일의 목차를 볼 수 있다.
  - jupyterlab_variableinspector : ipynb에서 선언한 변수의 이름과 값을 실시간으로 볼 수 있음
  - jupyterlab go to definition : 변수, 함수등 선언을 한 위치를 볼 수 있음
- Themes
  - mohirio/jupyterlab-horizon-theme
  - telamonian/theme-darcula  

## 설치 예정 extension list
- viwer / interaction
  - jupyter-matplotlib
  
## extension 설명
### jupyterlab git
![jupyterlab_git](https://github.com/jupyterlab/jupyterlab-git/raw/master/docs/figs/demo-0-10-0.gif)
- jupyter lab에서 ui를 통해 git을 사용할 수 있음
- 기본적으로 터미널에서 git 명령어를 통해 자세한 설정을 하고 ui를 통해서는 기본적인 동작만 사용
#### 사용방법
- git init : 상단의 메뉴 git -> init 
- 원격 저장소 추가 : 상단 git 메뉴 -> add remote repogitory

![git menu](img/git-menu.png)
- git init을 통해 생성한 폴더에서 branch 설정
  - 상단 git menu에서 `Open Git Repogitory in terminal`을 통해 터미널에 접속
  - `git branch --set-upstream-to=origin/<branch> master`
  
- commit : 좌측의 git 아이콘 -> commit 하고자 하는 파일을 체크박스로 선택 -> Summary 작성 -> commit 버튼 클릭
![git commit](img/git-commit.png)

- push / pull : 좌측 git 아이콘 -> 좌측 탭 상단에 아이콘 클릭(왼쪽:pull, 오른쪽:push) 
![git push/pull](img/git-push-pull.png)

- diff : 좌측 패널에서 파일 diff 아이콘 클릭시 파일 변경사항을 볼 수 있음
![git-fiff](./img/git-diff.png)

### jupyterlab_code_formatter
![code formatter](https://jupyterlab-code-formatter.readthedocs.io/en/latest/_images/demo.gif)
- 작성한 python code를 black, YAPF, ISORT로 포맷팅할 수 있음
- 사용 방법
  - commnad tab(ctrl+shift+c) -> JUPYTERLAB CODEFORMATER ->  apply <원하는 formmater> 클릭
  - 포맷팅하고자 하는 셀 오른쪽 클릭 -> Format Cell

### jupyterlab table of contents
![juptyerlab-toc demo](https://github.com/jupyterlab/jupyterlab-toc/raw/master/toc.gif)
- [git repogitory](https://github.com/jupyterlab/jupyterlab-toc/)
- ipynb와 md에서 작성되어 있는 markdown을 기준으로 table을 자동 생성
- ipynb에서는 cell의 code와 md에 작성된 내용을 미리 볼 수 있음

### jupyterlab variable inspector
![variable inspector demo](https://github.com/lckr/jupyterlab-variableInspector/raw/master/early_demo.gif)
- [git repogitory](https://github.com/lckr/jupyterlab-variableInspector)
- 사용 방법 
  - 확인하고 싶은 변수가 있는 노트북 오른쪽 클릭 -> open variable inspector

### ipywidget
![ipywidget demo](https://miro.medium.com/max/1400/1*AVw2xwbZls0MvCr5duz6IA.gif)
- [git repogitory](https://github.com/jupyter-widgets/ipywidgets)
- [ipywidget docs](https://ipywidgets.readthedocs.io/en/latest/)
- 사용 예제
  - [주피터 노트북에서 Interactive Widget 사용하기](https://junpyopark.github.io/interactive_jupyter/)
  - [Interactive Controls in Jupyter Notebooks](https://towardsdatascience.com/interactive-controls-for-jupyter-notebooks-f5c94829aee6)
- notebook에서 상호작용이 가능한 widget(table, graph 등)을 사용 가능

### go to definition
![go to definition demo](https://raw.githubusercontent.com/krassowski/jupyterlab-go-to-definition/master/examples/demo.gif)
- [git repogitory](https://github.com/krassowski/jupyterlab-go-to-definition)
- 사용 방법
  - 변수/함수 정의로 이동 : ALT + 왼쪽클릭
  - 이동 전으로 돌아가기 : ALT + O
- 내장 함수로 이동 불가. 

## Themes

### jupyterlab-horizon-theme
![horizon-theme](https://user-images.githubusercontent.com/29782314/69231444-6d1be500-0bcc-11ea-9bf6-635e425d69df.png)
### theme-darcula
![telamonian/theme-darcula](https://github.com/telamonian/theme-darcula/raw/master/darcula_preview.png)

### Theme 변경 방법
![](https://miro.medium.com/max/1400/1*iSSe-h-1QO9-g28vFi_Cpg.gif)

## 참고
- [blog-설치한 extension](https://nomad-programmer.tistory.com/19)
- [Yogayu/awesome jupyterlab extension](https://github.com/Yogayu/awesome-jupyterlab-extension)
- [mauhai/awesome-jupyterlab](https://github.com/mauhai/awesome-jupyterlab)