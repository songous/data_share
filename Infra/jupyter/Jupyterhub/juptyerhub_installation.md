# Kubernetes로 jupyterhub 설치하기

## 환경
- os : CentOS Linux release 7.7.1908 (Core) 
- minikube : v1.8.2
- docker : docker-ce 설치 / v19.03.8
- helm : v3.1.2

## 설치 목록
1. [Jupyter Enterprise Gateway](https://jupyter-enterprise-gateway.readthedocs.io/en/latest/)
2. [openLDAP와 phpLDAPadmin](https://extrapolations.dev/blog/2016/09/jupyterhub-kubernetes-ldap/)
3. [Jupyterhub](https://zero-to-jupyterhub.readthedocs.io/en/latest/)

## 1. Jupyter Enterprise Gateway 설치
1. enterprise-gateway.yaml을 작성
  - [enterprise-gateway.yaml](https://github.com/jupyter/enterprise_gateway/blob/master/etc/kubernetes/enterprise-gateway.yaml)은 github에 있는 yaml 파일 다운로드 한다.
  - 위 파일을 바로 적용하면 enterprise-gateway가 제공하는 모든 커널을 이용할 수 있다. 변경을 원할 시 EG_KERNEL_WHITELIST에 있는 value에서 사용할 kernel만 남겨두고 나머지 지우면 변경 가능하다.(이건 단순히 kernel을 보여주는 부분이라 KIP에서는 모든 이미지를 pulling하기 때문에 수정이 필요)
2. enterprise-gatewy 설치 : `kubectl apply -f enterprise-gateway.yaml`
  - ![eg-k8s-install](img/eg-k8s-install.png)
3. enterprise-gateway `kubectl get all -n enterprise-gateway`
  - ![](img/eg-k8s-check.png)
  - 위 명령어를 통해 설치된 pod, service를 확인가능
4. jupyterhub에서 Enterprise-gateway를 연결하기 위해서는 service의 cluster ip가 필요하다. pod가 죽더라도 service의 cluster ip는 변경되지 않기 때문에 별도로 수정하지 않더라도 사용가능하다.
  
## 2. openLDAP와 phpLDAPadmin 설치 및 설정

### openLDAP와 phpLDAPadmin
  - openLDAP는 ldap 기능을 제공하는 서비스
  - phpLDAPadmin은 openLDAP를 웹브라우저를 통해 쉽게 조작할 수 있게 해주는 서비스

### openLDAP와 phpLDAPadmin 설치하기
1. [ldap.yaml](ldap.yaml) 다운로드
2. ldap 설치하기
  - `kubeclt apply -f ldap.yaml`
  - ![](img/ldap-k8s-install.png)
3. ldap 설치 확인하기
  - `kubeclt get all -n ldap`
  - ![](img/ldap-k8s-check.png)
4. ldap가 정상 작동하는지 확인하기
  - 브라우저에 ip:port를 입력하면 php ldap admin에 접속 가능
    - ip는 minikube ip이고 port 번호는 ldap service의 port번호
  - ![](img/phpldapadmin-webpage.png)

### phpLDAPadmin을 통한 계정 설정
1. 로그인하기
  - 기본으로 설정되어 있는 관리자 계정은 admin이고 passwd 또한 admin이다
  - 왼쪽 탭에 login을 클릭하면 login 창이 열린다.
  - login DN에는 cn=admin,dc=example,dc=org를 입력 / password에는 admin 입력
    - ![](img/phpldapadmin-login.png)
  - login 시 첫 화면에서는 어떤 계정으로 로그인이 되어있는지와 ldap에 설정되어 있는 정보를 볼 수 있다. 
    - ![](img/phpldapadmin-login-first-page.png)
2. 조직추가(Organisational Unit 생성)
  - 왼쪽 탭에 `Create new entry here` 버튼 클릭한다.
    - 클릭 후 오른족 Templates 리스트 중 Generic: Organisational Unit을 클릭한다.
    - ![](img/phpldapadmin-add-ou-1.png)
  - Organisaional Unit 아래 택스트 창에 `juptyer` 입력 후 `Create Object` 버튼 클릭한다.
    - ![](img/phpldapadmin-add-ou-2.png)
  - 설정한 이름 확인 후 `Commit` 버튼 클릭한다.
    - ![](img/phpldapadmin-add-ou-3.png)
  - 왼쪽 탭에 `ou=jupyter`가 생성되었는지 확인한다.
    - ![](img/phpldapadmin-add-ou-4.png)
3. 사용자 계정 추가(Simple Security Object 생성)
  - 왼쪽 탭의 ou=jupyter를 클릭하면 화면에서 jupyter에 대한 정보 확인이 가능하다. 그 중에서 가운데에 있는 리스트 중 `Create a child entry`를 클릭한다.
    - ![](img/phpldapadmin-add-uid-1.png)
  - Template 선택 창에서 `Generic: Simple Security Object`를 클릭한다.
    - ![](img/phpldapadmin-add-uid-2.png)
  - User Name과 Password 입력 후 Create Object를 클릭한다.
    - ![](img/phpldapadmin-add-uid-3.png)
  - 설정한 value를 확인하고 `Commit`을 클릭한다.
    - ![](img/phpldapadmin-add-uid-4.png)
  - 왼쪽 탭에서 `o=ujupyter` 하위에 `uid=<user_name>`을 확인한다.
    - ![](img/phpldapadmin-add-uid-5.png)

    
## 3. JupyterHub 
### jupyterhub 설치
1. jupyterhub 설치를 위해 helm repogitory 추가
  - helm repogitory 추가 : `helm repo add jupyterhub https://jupyterhub.github.io/helm-chart`
  - helm repogitory 확인 : `helm repo list`
  
2. [jupyterhub_config.yaml](./jupyterhub_config.yaml) 파일 작성
  - `openssl rand -hex 32`를 통해 secretToken에 사용될 \<RANDOM-Hex\>값 생성
  - jupyterhub_config.yaml의 `proxy.secretToken`에 \<Random-Hex\> 값 입력
  - `singleuser.extraEnv.KG_URL`에 `http://<enterprise-gateway cluster ip>:8888` : enterprise gateway의 서비스 cluster ip를 입력
  - `auth.ldap.server.address: <jupyterhub-ldap>` : ldap adim이 아닌 ldap 서비스의 cluster ip를 입력
  
3. helm으로 jupyterhub 설치
  
  - `helm search repo -l jupyterhub` : 현재 설치 가능한 helm chart의 버전 정보를 확인
  - `kubectl create namespace hub` : hub 네임스페이스 생성
  - `helm install hub jupyrerhub -f config.yaml --version 0.9-0422521` : helm을 사용하여 hub라는 이름으로 설치, chart는 jupyterhub를 사용하고 version은 0.9-0422521를 사용한다. 네임스페이스는 hub를 사용
  - juptyerhub 삭제 : `kubectl delete namespace hub` 또는 `helm delete hub -n hub`를 사용

  
## 해야할 것
1. enterprise gateway에서 생성되는 커널은 별도의 pod이다. 따라서 현재 노트북에 있는 자료를 커널에서 직접 접근할 수 없다. 
  - kernel의 볼륨과 notebook의 볼륨을 서로 공유 또는 연결할 수 있는 방법 찾기
2. phpLDAP에서 계정 삭제 or jupyter hub에서 계정 삭제 시 k8s의 volume을 어떻게 삭제할 것인가 -> jupyterhub의 계정 관리
3. EG의 kernel에서 gpu 연동하는 방법
4. 사용자 volume 관리 -> 사용자가 로그아웃 후 로그인해도 이전에 작업 유지가 가능한지
  - 현재 nfs volume을 동적으로 생성하여 file 유지
5. notebook(git extension), kernel image(python package 설치) 최적화 
6. Jupyterhub 최신 버전의 기능 확인 (관리자 인터페이스 등)