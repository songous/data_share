# On-Demand Notebooks with JupyterHubm Jupyter Enterprise Gateway and K8s
## [Jupyter Blog 번역](https://blog.jupyter.org/on-demand-notebooks-with-jupyterhub-jupyter-enterprise-gateway-and-kubernetes-e8e423695cbf)

- 주피터 노트북은 데이터 과학자가 대화형 애플리케이션을 구축하고 빅데이터 및 AI 문제를 해결하기 위해 사용하는 "사실상 표준" 플랫폼이 되었다.
- 기업들에 의한 머신러닝과 AI의 도입이 증가함에 따라, 우리는 데이터 과학자와 데이터 엔지니어들을 위한 온디맨드 노트북을 제공하는 분석 플랫폼을 구축해야 할 필요성이 점점 더 많아지고 있다.
- 이 글은 주피터 노트북 스택에서 여러 구성요소를 배치하여 쿠버넷 클러스터에서 주피터허브와 주피터 엔터프라이즈 게이트웨이가 구동하는 온디맨드 분석 플랫폼을 제공하는 방법을 설명한다.

![](https://miro.medium.com/max/2116/1*F_jJ1nDSQgBhrkbsEXB93Q.png)

## On-Demand Notebooks Infrastructure

- 아래는 솔루션을 구축하는 데 사용할 주요 구성 요소와 각각의 설명을 참조
- JupyterHub는 우리가 찾고 있는 '서비스로서의' 느낌을 제공하는 단일 사용자 주피터 노트북 서버의 여러 인스턴스를 생성, 관리 및 프록시하는 다중 사용자 허브의 만들기를 가능하게 한다.
- Juppyter Enterprise Gateway는 커널이 자체 포드에서 실행될 수 있도록 함으로써 최적의 리소스 할당을 제공한다. 커널별 리소스는 라이프사이클에 따라 할당/배분되는 반면 노트북 포드는 최소한의 리소스를 가질 수 있다. 그것은 또한 커널의 기본 이미지가 선택이 될 수 있게 한다.
- Kubernetes는 탄력성 및 다양한 서비스 품질을 활용하여 컨테이너형 애플리케이션과 리소스를 쉽게 관리할 수 있다.


## JupyterHub Deployment

- JuppyterHub는 솔루션의 시작점이며, 각 사용자에 대한 개별 노트북 서버의 사용자 승인 및 프로비저닝을 관리한다.
- JupyterHub 구성은 config.yaml을 통해 수행되며, 다음 설정이 필요하다.

- 사용자 지정 사용자 이미지에서 가져온 사용자 지정 노트북 구성
``` yaml
hub:
  extraConfig: |-
    config = '/etc/jupyter/jupyter_notebook_config.py'
```

- 각 사용자의 노트북 서버를 인스턴스화할 때 사용할 도커 이미지 정의
- 노트북 서버를 주피터 Enterprise Gateway와 연결하여 원격 커널을 지원하는 데 사용되는 사용자 지정 환경 변수 정의

``` yaml
singleuser:
  image:
    name: elyra/nb2kg
    tag: 2.0.0
  extraEnv:
    KG_URL: <FQDN of Gateway Endpoint>
    KG_HTTP_USER: jovyan
    KERNEL_USERNAME: jovyan
    KG_REQUEST_TIMEOUT: 60
```
  - FQDN(Fully Qualified Domain name) : 전체 주소 도메인 네임 

- 전체 config.yaml은 아래와 같다

``` yaml
hub:
  db:
    type: sqlite-memory
#  extraConfig: |-
#    config = '/etc/jupyter/jupyter_notebook_config.py'
#    c.Spawner.cmd = ['jupyter-labhub']
proxy:
  secretToken: "xxx"
#ingress:
#  enabled: true
#  hosts:
#    - <FQDN Kubernetes Master>
singleuser:
  defaultUrl: "/lab"
  image:
    name: elyra/nb2kg
    tag: 2.0.0
#  storage:
#    dynamic:
#      storageClass: nfs-dynamic
  extraEnv:
    KG_URL: <FQDN of Gateway Endpoint> => EG service의 http://<cluster ip>:<port>
    KG_HTTP_USER: jovyan
    KERNEL_USERNAME: jovyan
    KG_REQUEST_TIMEOUT: 60
rbac:
  enabled: true
debug:
  enabled: true
```

- JupyterHub의 배포에 관한 상세한 내용은 [Zero to JupyterHub for Kubernetes](https://zero-to-jupyterhub.readthedocs.io/en/stable/)에서 볼 수 있다. 
- Kubernetes에 배포하기 위한 명령어는 아래와 같다
``` shell
helm upgrade --install --force hub jupyterhub/jupyterhub --namespace hub --version 0.7.0 --values jupyterhub-config.yaml
```

## Costom JupyterHub user image
- 기본적으로 JupyterHub는 보통의 노트북 서버 이미지를 배포할 것이다. 이 이미지는 kubernetes 이미지가 인스턴스화될 때 이미지에 의해 사용된 모든 자원을 할당해야 한다.
- 우리의 맞춤형 이미지는 커널을 자체 포드에서 시작할 수 있게 해 줄 것이며, 필요에 따라 자원을 할당하고 자유롭게 할 수 있기 때문에 더 나은 자원 배분을 촉진할 것이다. 이것은 또한 우리에게 서로 다른 노트북(예: Python과 TensorFlow를 사용하는 노트북, 다른 노트북은 Python과 Cafe2를 사용하는 노트북)에 대해 서로 다른 프레임워크를 지원하는 유연성을 제공한다.

- elyra-nb2kg custom image Dockerfile

``` python
FROM jupyterhub/k8s-singleuser-sample:0.7.0
# Do the pip installs as the unprivileged notebook user
USER $NB_USER
ADD jupyter_notebook_config.py /etc/jupyter/jupyter_notebook_config.py
# Install NB2KG
RUN pip install --upgrade nb2kg && \\
    jupyter serverextension enable --py nb2kg --sys-prefix
```

- notebook handler를 NB2KG의 handler로 대체하기 위한 주피터 노트북 사용자 지정 구성으로 노트북이 원격 커널을 지원하는 Enterprise Gateway에 연결할 수 있다.

``` python
from jupyter_core.paths import jupyter_data_dir
import subprocess
import os
import errno
import stat
c = get_config()
c.NotebookApp.ip = '*'
c.NotebookApp.port = 8888
c.NotebookApp.open_browser = False
c.NotebookApp.session_manager_class = 'nb2kg.managers.SessionManager'
c.NotebookApp.kernel_manager_class = 'nb2kg.managers.RemoteKernelManager'
c.NotebookApp.kernel_spec_manager_class = 'nb2kg.managers.RemoteKernelSpecManager'
# https://github.com/jupyter/notebook/issues/3130
c.FileContentsManager.delete_to_trash = False
# Generate a self-signed certificate
if 'GEN_CERT' in os.environ:
    dir_name = jupyter_data_dir()
    pem_file = os.path.join(dir_name, 'notebook.pem')
    try:
        os.makedirs(dir_name)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dir_name):
            pass
        else:
            raise
    # Generate a certificate if one doesn't exist on disk
    subprocess.check_call(['openssl', 'req', '-new',
                           '-newkey', 'rsa:2048',
                           '-days', '365',
                           '-nodes', '-x509',
                           '-subj', '/C=XX/ST=XX/L=XX/O=generated/CN=generated',
                           '-keyout', pem_file,
                           '-out', pem_file])
    # Restrict access to the file
    os.chmod(pem_file, stat.S_IRUSR | stat.S_IWUSR)
    c.NotebookApp.certfile = pem_file
```

- 위 문서는 `jupyter notebook --generate-config`에 의해 생성된 후 필요한 핸들러 오버라이드로 업데이트됨

``` python
c.NotebookApp.session_manager_class = 'nb2kg.managers.SessionManager'
c.NotebookApp.kernel_manager_class = 'nb2kg.managers.RemoteKernelManager'
c.NotebookApp.kernel_spec_manager_class = 'nb2kg.managers.RemoteKernelSpecManager'
```

## Jupyter Enterprise Gateway deployment

- Jupyter Enterprise Gateway를 통해 jupyter Notebook은 Kubernetes 클러스터를 비롯한 분배되어 있는 클러스터에서 원격 kernel들을 시작하고 관리할 수 있다.
- Enterprise Gateway는 Kubernetes 환경에서 간편하게 배포할 수 있는 Kubernetes 배포를 위한 YAML Descriptor를 제공한다.

``` shell
kubectl apply -f https://raw.githubusercontent.com/jupyter-incubator/enterprise_gateway/master/etc/kubernetes/enterprise-gateway.yaml
```
- kubernetes cluster의 모든 노드에 kernel 이미지들을 다운로드 해두는 것을 추천한다. 각각의 노드에서 kernel이 처음 시작돠리 때 delay와 time-out을 피할 수  있다.

``` shell
docker pull elyra/enterprise-gateway:dev
docker pull elyra/kernel-py:dev
docker pull elyra/kernel-tf-py:dev
docker pull elyra/kernel-r:dev
docker pull elyra/kernel-scala:dev
```