# jupyterhub
## jupyterhub 기본 구조

### subsystem
![jupyterhub architecture](https://jupyterhub.readthedocs.io/en/stable/_images/jhub-fluxogram.jpeg)
![](https://zero-to-jupyterhub.readthedocs.io/en/latest/_images/architecture.png
1. hub : jupyterhub의 중심
2. http proxy : 클라이언트의 요청을 수신
3. Spawners : 각 단일 사용자의 jupyter notebook 서버를 관리
4. authenticator : 사용자의 login 관리

### subsystem 상호작용
![](https://jupyterhub.readthedocs.io/en/stable/_images/jhub-parts.png)
- 허브가 proxy를 생성
- 프록시는 모든 요청을 허브로 전달
- 허브 내부에서 로그인 처리(Authenticator)를하고 단일 사용자 노트붃버를 생성(spawner)
- 허브는 URL에 따라 단일 사용자노트북 서버로 전달하도록 proxy를 구성

### Log-in 처리 과정
1. 로그인 데이터가 Authenticator에 전달된다
2. Authenticator는 로그인 정보가 유효할 때 사용자 이름을 반환된다
3. 로그인한 유저를 위한 단일 사용자 노트북 서버 인스턴스 생성된다
4. single-user server notebook server가 시작되면 프록시는 /user/[username]/* to the single-user notebook server 요청을 전달하라는 알림을 받는다
5. 암호화 된 토큰을 포함하는 쿠키가 /hub/에 설정
6. 브라우저가 /user/[username]으로 redirection된다. 그리고 요청은 single-user notebook server에서 처리된다

## LDAP 설치
- openldap와 phpldapadmin 설치


## jupyterhub 설치
### 사용가능 환경
- Linux/Unix :  jupyterhub는 Linux/Unix 기반 시스템을 지원
- window
  - jupyterhub의 default는 window를 지원하지 않는다.
  - window에서 작동 가능한 Spawner/Authenticator를 사용해야 jupyterhub를 사용 가능
  - Docker나 Linux VM 사용을 추천

### 설치시 고려사항
- 배포 시스탬(베어 메탈, 도커)
- 인증 방법(PAM, OAuth)
- single-user notebook server의 spawner (docker, batch)
- 추가 서비스(nbgrader 등)
- JupyterHub의 데이터베이스(Default SQLite; SQLAlchemy가 지원하는 database)


![](https://image.slidesharecdn.com/lbl-sep-2015-150919171801-lva1-app6891/95/jupyter-a-platform-for-data-science-at-scale-32-638.jpg?cb=1442683291)

### enterprise-gateway를 위한 이미지 생성
- elyra/nb2kg 
  - jupyhub/k8s-singleuser-sample:0.8.2 
  - serverextesion nb2kg, jupyterlab 추가
  - jupyter_notebook_config.py, start-nb2kg.sh 추가
  - start-nb2kg.sh : enterpirse gateway를 위한 환경변수 설정
``` shell
# start-nb2kg.sh의 일부
export NB_PORT=\${NB_PORT:-8888}
export GATEWAY_HOST=\${GATEWAY_HOST:-localhost}
export KG_URL=\${KG_URL:-http://\${GATEWAY_HOST}:\${NB_PORT}}
export KG_HTTP_USER=\${KG_HTTP_USER:-jovyan}
export KG_REQUEST_TIMEOUT=\${KG_REQUEST_TIMEOUT:-30}
export KERNEL_USERNAME=\${KG_HTTP_USER}
...
```



### volume

### 해야할 것

1. kernel에서 사용자가 install한 패키지 유지

2. 계정 관리
  - phpLDAP에서 계정 삭제 or jupyter hub에서 계정 삭제 시 k8s의 volume을 어떻게 삭제할 것인가 -> jupyterhub의 계정 관리

3. enterprise-gateway kernel 이미지
  - 커스텀 커널 이미지
  - 외부 gpu 사용가능 서버에서 커널 생성
  
4. notebook 이미지 생성
  - oc cli
  - podman
  - 기본적인 shell 명령어
  - jupyter notebook extension : git 등
  

5. enterprise gateway 
  - min/max resource 설정
  - 

