# JupyterLab Extension

## 설치할 extension

1. jupyterlab-git
  - url : https://github.com/jupyterlab/jupyterlab-git
  - isntall
    pip install --upgrade jupyterlab-git
    jupyter lab build
  - jupyter serverextension enable --py jupyterlab_git
 
2. juptyerlab-LSP
- url : https://github.com/krassowski/jupyterlab-lsp
- Prerequisites
  - jupyterlab >=2, < 3.0.0.0a0
  - install : pip install jupyter-lsp
  - jupyter labextension install @krassowski/jupyterlab-lsp
  
3. debugger
  - xeus python 필요
  
4. jupyterlab html
  - https://github.com/mflevine/jupyterlab_html
  - jupyter labextension install @mflevine/jupyterlab_html
  - html viewer

  
### jupyterlab-git
- 기존에 사용했던 elyra/nb2kg 이미지는 juptyerlab version 1.1.1이라 dependency에 맞지 않음
- 새로운 base image에 git extension 추가
-