# CentOS7에 Anaconda를 이용하여 Jupyterhub 설치하기

## CentOS Anaconda 설치
1. 설치파일 download 
2. sh 설치파일.sh
3. source .bashrc

## jupyterhub 설치
1. conda create -n jhub python=3.7
2. conda activate jhub
3. conda install -c conda-forge jupyterhub
4. conda install notebook
5. jupyterhub  
  - `--ip 0.0.0.0` : 외부에 공개
  - `--port 8000` : default 포트가 8000


### jupyterhub 설정
- `jupyterhub --generate-config` 사용하여 `jupyterhub_config.y-py` 생성
``` python
c = get_config()
# jupyterHub의 기본 ip,url, 프로토콜, base_url을 설정
c.JupyterHub.bind_url = 'http://127.0.0.1:8000/jupyter' 
```

## nginx
- local에서 실행되는 jupyterhub를 외부에 액세스를 할 수 있게하기 위해서 nginx 사용 

### 설치
- yum 외부 저장소 추가 : `vi /etc/yum.repos.d/nginx.repo`
- 설치 : `yum install -y nginx`
- jupyter hub 관련 설정 추가 : `vi /etc/nginx/conf.d/default.conf`

``` sh
server{
    listen    80;
    server_name localhost;    
    location /jupyter {
        proxy_pass http://127.0.0.1:8000;
        proxy_http_version 1.1;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header Host \$(host);
        proxy_set_header X-Forwarded-For \$(proxy_add_x_forwarded_for);
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection "upgrade";
    }
}
```

- 외부에 리눅스 port open
  - 리눅스 포트 개방
    - `sudo iptables -I INPUT -p tcp --dport 80 -j ACCEPT`
    - `sudo iptables -I OUTPUT -p tcp --dport 80 -j ACCEPT`
  - 포트 개방 삭제
    - `sudo iptables -D INPUT -p tcp --dport 80 -j ACCEPT`
    - `sudo iptables -D OUTPUT -p tcp --dport 80 -j ACCEPT`

### error

- centos 7 에 nginx 설치시 접근권한으로 인해 에러 발생
- audit 관련된 에러
- 수정하기 위해서 audit에 규칙 추가를 해야함
- `yum install policycoreutils-python`

``` shell
sudo cat /var/log/audit/audit.log | grep nginx | grep denied | audit2allow -M swnginx
sudo semodule -i swnginx.pp
```
- nginx 재시작 : `sudo systemctl restart nginx.service`

## Jupyter Enterprise Gateway
### 설치 
- conda를 통해 jupyter_enterprise_gateway 설치
  - `conda install -c conda-forge jupyter_enterprise_gateway`
- nb2kg 설치
  - 패키지 설치 : `conda install nb2kg -c conda-forge`
  - jupyter 확장 등록 : `jupyter serverextesion enable --py nb2kg --sys-prefix`
 
### 시작
- `jupyter enterprisegateway --ip=0.0.0.0 --port_retries=0`
- `--ip=0.0.0.0`은 퍼블릭 네트워크에 enterprise gateway 노출, 설정하지 않으면 127.0.0.1:8888

- enterprise-gateway의 ip:port 환경변수 등록 : `export KG_URL=http://127.0.0.1:8888`
- `jupyter notebook --generate-config`를 통해 jupyter_notebook_config.py 생성
``` python
import nb2kg
from nb2kg import managers
c = get_config()
c.NotebookApp.session_manager_calss = 'nb2kg.managers.SessionManager'
c.NotebookApp.kernel_manager_class = 'nb2kg.managers.RemoteKerenelManager'
c.NotebookApp.kernel_spec_manager_class = 'nb2kg.managers.RemoteKernelSpecManager'
```
- jupyterhub_config.py
``` python
import os
c = get_config
os.environ['KG_URL']='http://<enterprise-gateway endpoing>:<port>'
c.Spawner.env_keep.append('KG_URL')
c.JupyterHub.bind_url = Unicode('http://127.0.0.1:8000/jupyter')
```