import os

c = get_config()

# jupyterhub의 기본 주소를 설정
c.JuputerHub.bind_url = "http://127.0.0.1:0000/jupyter"

# juptyer enterprise-gateway의 주소를 KG_URL로 설정
## kubernetes에 서비스로 올려져 있다면 nodeport의 경우 <minikube ip>:port로 설정
os.environ['KG_URL'] = "http://192.168.137.60:31597"
c.Spawner.env_keep.append('KG_URL')