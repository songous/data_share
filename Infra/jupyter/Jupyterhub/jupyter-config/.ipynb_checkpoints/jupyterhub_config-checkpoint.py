import nb2kg
from nb2kg import managers

c = get_config()

c.NotebookApp.session_manager_class = 'nb2kg.mansgers.SessionManager'
c.NotebookApp.kernel_manager_class = 'nb2kg.mansgers.RemoteKernelManager'
c.NotebookApp.kernel_spec_managerr_class = 'nb2kg.managers.RemoteKernelSpecManager'
