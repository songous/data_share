# Jupyterhub
---------------------------------
## Arcitect
------------------------------------------
![jupyterhub architecture](img/jhub_with_k8s.png)


## Estimate Memory / CPU / Disk needed

- memory    
    - `RecommendedMemory = (Maxconcurrentusers×Maxmemperuser)+128MB`
    - Maximum Concuurent User : 최대 동시 사용자
    - Maximum mem per user : 사용자당 최대 허용 메모리
- CPU
    - `RecommendedCPU=(Maxconcurrentusers×MaxCPUusageperuser)+20%`
- Disk size
    - `RecommendedDiskSize=(Totalusers×Maxdiskusageperuser)+2GB`
    
## DigitalOcean kubernetes에 설치
--------------------------------------------------
### 연동 방법
- doctl 설치 후 local pc와 연동
    - `doctl auth init` : do와 연동
    - token은 digital ocean의 api탭에서 확인가능
- ~/.kube에 config 파일 생성, 파일 이름이 config. 확장자 없음
    - do에서 생성한 클러스터에 관한 config 파일 다운로드
    - `kubectl config view --kubeconfig="다운받은 yaml파일"` : yaml을 config로 이름 변환 했을 때 되는지도 시도<br>
      `doctl k8s cluster kubeconfig show bindertime-k8s > ~/.kube/config`
      
### DigitalOcean volume 만들기
- StorageClass : 기존 `do-block-storage`
- PersistVolumeClaim : 이하 yaml 파일 만들기

``` yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: jhub-csi-pvc
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 5Gi
  storageClassName: do-block-storage
```
- `kubectl create -f <pvc yaml 파일이름>`

## local 설치(conda)

## Helm을 사용하여 설치하기
- openssl을 사용하여 보안 토큰 발급<br>
`openssl rand -hex 32`

- repo 등록<br>
`helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/`<br>
`helm repo update`
- 설치<br>
`helm install jhub jupyterhub/jupyterhub -n jhub --values config.yaml`<br>
`helm install <name> <repo> -n <namespace_name> --valuse <config file>`

- config.yaml 내용

``` yaml
hub:
  db:
    type: sqlite-memory
proxy:
  service:
    type: NodePort
  secretToken: "c44f701e39aec9d6a9b3cd9ba6ec98fb889b8a56c2a6dc7efb9fb1c1e2abb805"

singleuser:
  defaultUrl: "/lab"
  image:
#     name: jupyter/base-notebook
#     tag: latest
    name: elyra/nb2kg
    tag: 2.0.0
  extraEnv:
    KG_URL: http://<EG servicd cluster ip>:8888
    KG_HTTP_USER: jovyan
    KERNEL_USERNAME: jovyan
    KG_REQUEST_TIMEOUT: 60
rbac:
  enabled: true
debug:
  enabled: true

# ldap로 변경 예정
auth:
  type: dummy
  dummy:
    password: 'mypassword'
  admin:
    access: true
    users:
      - admin
  whitelist:
    users:
      - test1
```

- classic jupyter notebook에서만 admin으로 들어갈 수 있음. 기본 url을 lab으로 바꿔도 admin 계정은 변경 안 됨.


### 사용자 인증 관리
- 인증 방식은 크게 3가지
  - OAuth2
  - LDAP(Lightweight Directory Access Protocol)
  - whitelist : 미리 지정한 ID/PW를 사용하여 인증하는 방식
- helm으로 설치한 jupyterhub의 기본 인증 방식은 whitelist.
- OAuth2로 인증
  - jupyterhub는 여러가지 third-party OAuth provider을 통한 인증을 지원
  - Github, Google, CILogon 등
  - google 
    - 구글 인증(G Suite의 기능)을 이용하여 인증
    - jupyterhub에 도메인네임이 설정되어 있어야 사용 가능
  - gitlab
    - OAuth2 지원, redirect uri가 필요 
    - user profile > application
    - [gitlab OAuth2 Docs](https://docs.gitlab.com/ee/api/oauth2.html)
- LDAP로 인증 지원
  - 참고 : [Jupyter Hub on Kubernetes with LDAP](https://extrapolations.dev/blog/2016/09/jupyterhub-kubernetes-ldap/)
  
### 인증을 위한 LDAP 설치
- LDAP(Lightweight Directory Access Protocol)란?
  - LDAP는 네트워크 상에서 어떠한 정보(전화ㅏ번호, 주소, 조직, 파일, 계정 등)을 쉽게 찾아 볼 수 있게 하는 소프트웨어 프로토콜이다.
  - LDAP는 디렉토리 서비스를 의미하기도 하고 LDAP 프로토콜을 의미하기도 하는데, LDAP 디렉토리 서비스는 LDAP 프로토콜의 구현체이다
  - LDAP는 
  
### 알아볼 것
- 영구 storage 세팅 
- 기본 환경 세팅 : anaconda, git 연동, package 설치
    - jupyter terminal에서 package 추가 설치 가능한지 확인
- user 관리 : ID, PW 등... (LDAP?)




## 환경 커스터마이징
-----------------------------------------------
- 기본 제공 Dockerfile
- Customized Docerfile 
    - [Docker stack](https://github.com/jupyter/docker-stacks/)을 이용하여 

## error
--------------------------------------------------
- `insufficient memory` : 메모리 부족
    - local 환경: 기존 minikube를 delete하고 재생성
    - DigitalOcean : 메모리 업그레이드 노드 추가
    
    
- `API request failed (500) Spawner failed to start [status=1]. The logs for admin may contain details.`
    - `kubectl logs <hub-pod-name>`을 통해 log확인
    - `TypeError: '<' not supported between instances of 'NoneType' and 'datetime.datetime'일 때`
        -  타임 스탬프 필드가 없는 이벤트의 관련 에러. 
        - K8s v1.16과 python api가 호환되지 않아서 발생
        - [git issue]( https://github.com/jupyterhub/kubespawner/issues/354) 확인
        - [gitter](https://gitter.im/jupyterhub/jupyterhub?at=5d8c70297ed7fe1f1a5362fe) 확인
        - `kubectl patch deploy -n $NAMESPACE hub --type json --patch '[{"op": "replace", "path": "/spec/template/spec/containers/0/command", "value": ["bash", "-c", "\nmkdir -p ~/hotfix\ncp -r /usr/local/lib/python3.6/dist-packages/kubespawner ~/hotfix\nls -R ~/hotfix\npatch ~/hotfix/kubespawner/spawner.py << EOT\n72c72\n<             key=lambda x: x.last_timestamp,\n---\n>             key=lambda x: x.last_timestamp and x.last_timestamp.timestamp() or 0.,\nEOT\n\nPYTHONPATH=HOME/hotfix jupyterhub --config /srv/jupyterhub_config.py --upgrade-db\n"]}]'`
        - 위 명령이 적용이 되지 않을 때 
            - kbuectl edit deployment hub
            - `hub.spec.template.spec.containers[0].command` 수정
            - 아래 내용으로 변경
            
``` yaml
containers:
  - command:
    - bash
    - -c
    - |
      mkdir -p ~/hotfix
      cp -r /usr/local/lib/python3.6/dist-packages/kubespawner ~/hotfix
      ls -R ~/hotfix
      patch ~/hotfix/kubespawner/spawner.py << EOT
          72c72
      <             key=lambda x: x.last_timestamp,
      ---
      >             key=lambda x: x.last_timestamp and x.last_timestamp.timestamp() or 0.,
      EOT
      PYTHONPATH=$HOME/hotfix jupyterhub --config /srv/jupyterhub_config.py --upgrade-db
     env:
    - name: PYTHONUNBUFFERED
      value: "1"
    - name: HELM_RELEASE_NAME
```

- TypeError: '<' not supported between instances of 'datetime.datetime' and 'NoneType'
  - 관련 이슈 : https://github.com/jupyterhub/kubespawner/issues/354
  -  K8s API의 주요 변경으로 인한 경우 K8s API의 주요 변경으로 인한 경우 수정 프로그램은 업스트림 Python 클라이언트 패키지를 기다려야합니다
  - helm chart version  0.9.0-beta.3에서 수정됨
  - 해결법
    - kubectl patch 명령
    - `kubectl patch deploy -n $NAMESPACE hub --type json --patch '[{"op": "replace", "path": "/spec/template/spec/containers/0/command", "value": ["bash", "-c", "\nmkdir -p ~/hotfix\ncp -r /usr/local/lib/python3.6/dist-packages/kubespawner ~/hotfix\nls -R ~/hotfix\npatch ~/hotfix/kubespawner/spawner.py << EOT\n72c72\n<             key=lambda x: x.last_timestamp,\n---\n>             key=lambda x: x.last_timestamp and x.last_timestamp.timestamp() or 0.,\nEOT\n\nPYTHONPATH=$HOME/hotfix jupyterhub --config /srv/jupyterhub_config.py --upgrade-db\n"]}]'`


## 참고
----------------------------------------------
- [jupyterhub for kubernetes](https://zero-to-jupyterhub.readthedocs.io/en/latest/index.html)
- https://zero-to-jupyterhub.readthedocs.io/en/latest/index.html
- https://zero-to-jupyterhub.readthedocs.io/en/latest/customizing/user-environment.html