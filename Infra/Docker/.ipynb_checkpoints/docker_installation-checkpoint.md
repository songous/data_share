# Docker 설치
## ubuntu
- 이하 명령어를 사용하여 설치
- `sudo apt-get update`
- `sudo apt-get install docker.io`
- `sudo ln -sf /usr/bin/docker.io /usr/local/bin/docker` : 실행 파일에 대한 링크 설정

## CentOS
- `sudo yum install docker`
- `sudo service docker start` : Docker 서비스 실행
- `sudo chkconfig docker run` : 부팅시 자동 실행 설정

## Windows 10
- 환경
  - Window 10 x64 pro or enterprise 필요
    - 자세한 사항은 [링크](https://docs.docker.com/docker-for-windows/install/) 참조
  - CPU 가상화 기능이 활성화 되어 있어야 함
    - ![](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=http%3A%2F%2Fcfile8.uf.tistory.com%2Fimage%2F99BF3D4E5D4C23CA14DD3B)
    - 활성화 되어 있지 않다면 BIOS에서 활성화
  - Hyper-V 필요
    - 'Windows 기능 켜기/끄기'에서 Hyper-V 옵션 활성화
    - ![](https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=http%3A%2F%2Fcfile24.uf.tistory.com%2Fimage%2F99DB1A485D4C254427C929)


- 설치
  - 설치 파일 다운로드 : [다운로드 링크](https://docs.docker.com/docker-for-windows/install/#download-docker-for-windows)
  - 설치 파일 실행하여 단계에 따라 설치