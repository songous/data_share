# AWS Batch

- 완전 관리형 배치 컴퓨팅 서비스
-  별도의 설치 및 운영할 SW가 없으며, 배치 작업용 인프라 설정, 확장 및 모니터링 작업
- 스토리지 및 데이터 저장을 위해 S3, DynamoDB에 저장 가능
- EC2 인스턴스 및 EC2 Spot 인스턴스를 통한 저렴한 배치 작업 가능
- 배치 작업이 완료 후 인스턴스는 종료 된다.
- Job의 dependency를 이용하여 워크플로우 관리도 가능

## 주요 특징

- 세분화된 작업 장식 정의. vCPU/메모리 요구사항, Amazon ECS 기반의 애플리케이션 작업 정의 및 IAM 역할 등

## 기본 용어

- 작업(Job)
  - EC2에서 실행되는 컨테이너화 된 애플리케이션으로 실행되는 작업 단위
  - Job role : IAM role
  - Container image : ECR or AMI
- 작업 정의(Job Definition)
  - 작업 이미지, 작업 실행환경
- 작업 대기열(Job Queue)
  - 생성된 작업은 작업 대기열로 전송되며, 컴퓨팅 리소스를 예약 가능할 때까지 대기하면서 우선 순위에 따라 작업
  - Compute environment
- 컴퓨팅 환경(Compute Environment)
  - Job queue에 포함될 instance에 대한 정의를 하는 곳
- AWS Lambda : Batch job의 dependancy 적용을 위한 간단한 코드
- CloudWatch : Batch job을 주기적으로 수행하기 위한 trigger  

## console menu
- Dashboard : 정의된 Job queue와 Computer environment의 목록을 볼 수 있으며, 각각의 Job queue별로 Job의 Status(submitted, pending, runnable, starting, running, failed, succeeded) 상태를 볼 수 있다
- Jobs : Job queue마다 Status 별로 job의 목록을 복수 있다.
- Job Definitions : 정의된 Job을 볼 수 있고, 생성 및 삭제가 가능하다.
- Job queues : 정의된 job queue 확인, 생성 및 삭제가 가능하다. 삭제시 Disalb -> Delete 순으로.

## 활용 아키텍처
![](https://idk.dev/wp-content/uploads/2019/05/Creating-an-AWS-Batch-environment-for-mixed-CPU-and-GPU.png)
- 비용 효율적 작업 분배
- Step Function을 이용한 배치 작업 관리
  - Step function으로 워크플로를 작성하고 각 작업에서 lambda를 실행
- Array Jobs을 통한 병렬 작업 처리

## 참고
- [Batch 예제 github](https://github.com/dejonghe/aws-batch-example)
- [AWS Batch 손쉬운 일괄 처리 작업 관리하기](https://www.youtube.com/watch?v=-ox2Tqs3m5s)
- [Batch 튜토리얼](https://devstarsj.github.io/cloud/2018/11/24/aws-batch-tutorial/)
- [AWS Batch를 이용한 분산 병렬 딥러닝 학습 #1](https://coffeewhale.com/deep-learning/aws/batch/docker/2018/05/19/deeplearning-aws-batch1/)
- [AWS Batch 사용법](https://blog.leedoing.com/191)