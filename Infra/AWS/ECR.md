### ECR (Elasic Container Registry)
- 완전관리형 도커 레지스트리 서비스
- AWS IAM을 사용하여 권한 관리
- docker CLI 명령어를 사용하여 접근 가능
- AWS S3에 저장하여 높은 가용성 및 안정성을 가진다.

#### 요금
- 스토리지 : 월별 GB당 0.1USD
- 데이터 수신 : 0 USD
- 데이터 송신
![요금(서울리전)](img/ECR_Pricing.PNG)

#### ECR 사용에 필요한 것
- 권한을 가진 사용자
- ECR에 생성된 Repository
- login id를 가지고 올 aws-cli

#### ECR에 push하기
##### Step 1. AWS에 권한을 가진 사용자 추가 
1. IAM에 접속  
  - 사용자가 존재하면 권한만 추가(정책 : AmazonEC2ContainerRegistryFullAccess)
  - 사용자가 없다면 IAM-> 액세스관리 -> 사용자 -> 사용자 추가
  
  
2. 사용자 이름 및 액세스 유형 선택
  ![사용자추가_이름_액세스](./img/aws_add_user_1.PNG)
  - 원하는 사용자 이름 작성
  - 액세스 유형 : 프로그래밍 방식 액세스 


3. 권한 추가 <br>
  ![사용자추가_권한](./img/aws_add_user_auth.png)
  - 권한설정에서 기존 정책 직접 연결 선택
  - 정책 AmazonEC2ContainerRegistryFullAccess로 선택
    - 권한에 대한 상세 내역은 아래 링크 참고
    - [ECR 권한 Docs](https://docs.aws.amazon.com/ko_kr/AmazonECR/latest/userguide/ecr_managed_policies.html)
  - 생성시에 발급된 access key ID와 key 복사


4. 액세스 키 발급
  ![액세스 키 발급](https://t1.daumcdn.net/cfile/tistory/9996E94A5C68B73A36)
  - 마지막 생성 완료 시점에서 key ID와 key 복사를 못했다면 새로운 access key 발급
  - 발급 후에는 복구 불가. cvs 파일 다운로드를 미리 해두기 

##### STEP 2. ECR repogitory 생성
1. ECR 접속 -> 리포지토리 생성
![](./img/aws_ecr_repo_create.PNG)

2. 리포지토리 이름 설정 
  - 리포지토리 이름은 자신이 생성할 이미지에 맞춰 설정

##### 3. docker login과 push
1. aws-cli가 설치되어 있지 않다면 설치
  - Linux CentOS / RedHat 계열
    -`sudo yum install aws-cli`
    - 설치 확인 : `aws --version`

2. AWS ECR registry login
  - `aws configure` : 액세스 키 설정
    - 이전에 복사해둔 Access Key Id와 Key 복사 붙여넣기
    - resion name과 output 포맷은 설정하지 않아도 됨
  - `aws ecr get-login --no-include-email --region ap-northeast-2`
    - 출력 결과를 복사 하여 실행
    - `docker login -u AWS -p eysdf....` : password 토큰이 출력
  - ECR repo에 push
    - `docker tag <기존 img 이름> <ECR repo의 URI>` : 리포지토리 생성후 URI를 확인 가능
    - `docker push <repo URI>`
  - 결과 확인은 ECR 콘솔이나 명령어로 확인 가능
    - 명령어 : `aws ecr list-images --repository-name=<repo 이름>` : uri가 아닌 repo의 이름을 입력, 만약 region을 default로 설정하지 않았다면 `--region <region>` 인자 추가