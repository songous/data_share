### VPC
#### 특징
- 독립된 네트워크 환경을 구성
- 논리적으로 격리된 공간을 프로비져닝
- 직접 가상 네트워크를 정의하여 사용 가능
- 현재 대부분의 기능이 VPC에 의존적

#### 구성요소
- 1VPC
  - 이름과 CIDR 블록을 가짐
  - CIDR 블록은 IP의 범위를 지정하는 방식. IP 주소와 /로 구성
  - 192.168.0.0/n : 2^(32-n)개의 IP주소를 가짐
  - n의 최대값은 16으로 최대 65536개의 IP를 사용할 수 있다.
- n 서브넷
- 1 라우트 테이블
- 1 네트워크 ACL
- 1 시큐리티 그룹
- 1 인터넷 게이트웨이
- 1 DHCP 옵션셋

#### VPC NAT GateWay
- 프라이빗 서브넷이 외부와의 연결이 필요할 때 사용
- 위의 그림을 보면 프라이빗 서브넷의 앱서버를 웹서버들이 설치된 퍼블릭 서브넷을 이용하여 인터넷을 사용하기 위해 VPA NAT GateWay를 사용하는 것을 볼 수 있다.

### EIP (Elastic IP)
- 고정 public IP를 할당해 주는 기능 기능
-  EC2의 Stop Start 시 IP주소가 변경되는데 이 때 EIP를 사용하여 고정된 IP를 사용할 수 있다.


### 참고
- [아마존 네트워크 기초 VPC](https://www.44bits.io/ko/post/understanding_aws_vpc)
- [AWS network 이론](https://galid1.tistory.com/219?category=765373)