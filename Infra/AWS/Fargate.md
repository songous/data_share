# Fargate
![](https://d2908q01vomqb2.cloudfront.net/da4b9237bacccdf19c0760cab7aec4a8359010b0/2017/11/29/Picture1.png)

## 개요
- Fargate는 컨테이너에 적합한서버리스 컴퓨팅 엔진으로  ECS와 EKS를 모두 지원
- 서버를 프로비저닝하고 관리할 필요없이 컨테이너를 실행하는데 필요한 비용만 지불한다.

## 장점
- 프로비저닝, 패치 적용, 클러스터 용량 관리, 인프라 관리가 필요없다.


- 