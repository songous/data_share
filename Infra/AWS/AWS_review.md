# ECS와 Gitlab-CI 사용법 검토
## Use Case 이미지
![](https://blobs.gitbook.com/assets%2F-LgLv25e2BrxRC5m6flh%2F-LiYlfgxpWkNrA8-5Qwh%2F-LiYnuZzvpY8uIcbAV8C%2FScreen%20Shot%202019-06-29%20at%208.43.58%20PM.png?alt=media&token=a476b683-e521-416d-acae-0d0b21e8080c)
- ECS 서비스를 gitlab ci를 통해 CI/CD
- gitlab ci는 docker image를 빌드하고 테스트. 그리고 서비스에 적용시에 aws-cli를 통해 ECS의 작업과 서비스를 업데이트한다.

## ECS 검토 사항

### 1. EC2 인스턴스(컨테이너 인스턴스)
- ECS에서 EC2를 사용한다면 해당 사용 EC2에 대한 요금만 부과. 따라서 EC2를 작업의 동작 여부에 따라 생성/삭제가 필요
  - EC2 auto scaling을 이용한 EC2 인스턴스 관리 가능
      - 오토스케일링 그룹을 사용하면 작업이 필요할 때 목표 용량을 필요한 인스턴스의 수로 변경하여 생성하고 사용하지 않을 때는 0으로 하여 인스턴스를 삭제
      - `autoscaling set-desired-capacity --auto-scaling-group-name <auto scaling group name> --desired-capacity <num>`
  - **ECS의 용량 공급자기능을 이용한 인스턴스 관리**
    - ECS의 용량 공급자를 이용하여 EC2 인스턴스 생성 가능
    - 생성 후 삭제는 전략을 통해 가능한지 확인 중
    - 생성 후 삭제가 되지 않으면 ASG를 통해 인스턴스 삭제
  - **SQS를 사용하여 일정이상 유휴 상태인 인스턴스를 삭제**
    - SQS 대기열 메시지와 동일한 인스턴스를 추가하여 확장
    - SQS 대기열이 비어 있으면 인스턴스를 0으로 설정
    - 일괄작업 시작시 ASG 인스턴스 보호 활성화 및 마지막에 비활성화
    - 단점은 인스턴스 당 하나의 배치 작업으로 제한
  - 확인 해야할 사항
    - gitlab-ci를 통해 생성, 작업 실행, 삭제가 순차적으로 사용 가능한 지 확인해야 함.
    - 하나의 클러스터에 여러 개의 오토 스케일링 그룹을 사용 가능한지와 관리 방법
    - **다른 생성 / 삭제 방법이 있는지 확인 중**
- EC2 인스턴스의 타입이 다양하여 필요에 따라서 type을 선택하여 사용 가능
  - ECS에서 EC2 인스턴스를 사용한다면 미사용된 자원에 대한 고려가 필요. 한 EC2 인스턴스의 자원 사용을 최대화해야 할 필요가 있음
  
### 2. Fargate
- Fargate는 동작할 때에만 요금이 부과되어 서비스를 등록하고 이후에 사용을 하지 않는 다면 요금이 없음.
- 일반적인 docker 이미지를 사용한 작업에는 문제가 없지만 CPU/메모리가 최대 4vCPU/30GB로 이 이상을 초과하는 작업이 필요할 때는 사용 불가. 또한 GPU도 사용이 불가능하다.

### EC2 vs Fargate 비교
- ECS에서 Fargate와 EC2는 하나의 클러스터에서 모두 사용 가능하다. 따라서 용도에 따라 선택하여 사용
  - fargate는 적은 리소스가 필요한 작업일 때 사용
  - EC2는 GPU 등의 리소스가 필요한 작업일 때 사용

### 3. AWS Batch
- AWS Batch 기능
  - Fargate or EC2 중 하나를 선택하여 동적으로 프로비저닝하는 서비스
  - 컨테이너 이미지 보관비용 이외에는 추가 비용 없음
  - 사용예시 : 정기적인 크롤링

## Gitlab CI 검토 사항 
- gitlab runner를 통한 서비스 관리
  - aws-cli를 통하여 서비스나 작업 업데이트, 서비스 동작 관리
- gitlab runner의 위치
  - gitlab runner용 ec2 인스턴스 : ec2에 대한 요금만 적용. push를 자주하여 테스트를 진행하면 fargate보다는 좋은 선택
  - fargate를 이용해 생성 : 아직 확인 안됨
  - 로컬 서버 : 클라우드 사용에 따른 추가 비용이 없다는 장점이 있다.  
- 위치에 따른 gitlab runner 사용 방법
  - 로컬 서버 : 로컬에 직접 설치하거나 docker container로 설정. 
  - AWS
    - EC2 인스턴스 : 로컬 서버와 비슷하게 사용할 수 있음.
    - ECS
      - 컨테이너 인스턴스 : EC2와 동일하나 ECS의 서비스로 관리하는 방법 확인 중
      - ECS에서 EC2 인스턴스를 사용하여 서비스로 gitlab-runner를 사용한다면 gitlab-runner의 설정 관리를 자동화 해야함. 컨테이너가 정지 후 재생성 시 gitlab runner의 설정 정보를 이전과 동일하게 하여 연결을 유지해야 함. 안될 경우 runner를 통한 CI가 실패시 ECS 관리자가 ssh로 접속하여 URL, token 등 정보를 재등록 해주어야 함
      - Fargate : 사용가능한지 확인 중. 관련 issue에서 기능 추가를 요청하는 사용자들이 있음. 현재는 사용이 불가능한 것으로 보임.
      


## 확인 중

- 컨테이너 인스턴스 관리 방향
  - ECS의 용량 공급자를 이용
    - autoscaling을 통한 자동 생성확인
    - 삭제 방법 확인 필요
  - AWS Batch를 사용항 배치 워크로드
  - SQS를 이용한 인스턴스 확장/축소 정책
  - airflow에서 작업을 실행할 때 실행할 자원이 있는지 확인 후 생성/삭제
  - gitlab runner에서 ec2를 생성하고 airflow에서 오토스케일링 관리
    - 인스턴스 생성 중일 때 작업을 어떻게 관리해야 하는지?
  
- ECS 운영 정책 검토
    - Fargate와 GPU 자원 사용 가능한 EC2 사용에 대한 운영 정책
- ECS에 서비스를 여러개 등록했을 때 실행에 제약사항 있는지 확인
  - ECS 예약된 작업 역할 확인
  