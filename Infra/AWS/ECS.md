# ECS 정리
## 아키텍처
![ECS Architectureecs_architecture](img/ecs_architecture.png)

## 사전 지식
1. [VPC](VPC.md) : ECS 클러스터의 네트워크 구성에 사용
2. [ECR](ECR.md) : ECS 작업(task)에서 사용되는 컨테이너를 생성하는데 사용

### ELB(Elastic Load Balancer)
### 종류 
- ELB에는 총 3가지 종류가 있음
  - ALB (Application Load Balancer)
    -  개방형 시스템 간 상호 연결(OSI) 모델의 일곱 번째 계층인 애플리케이션 계층에서 작동합니다. 로드 밸런서는 요청을 받으면 우선 순위에 따라 리스너 규칙을 평가하여 적용할 규칙을 결정한 다음, 규칙 작업의 대상 그룹에서 대상을 선택합니다. 애플리케이션 트래픽의 콘텐츠를 기반으로 다른 대상 그룹에 요청을 라우팅하도록 리스너 규칙을 구성할 수 있습니다. 대상이 여러 개의 대상 그룹에 등록이 된 경우에도 각 대상 그룹에 대해 독립적으로 라우팅이 수행됩니다.
  - NLB (Network Load Balancer)
    - 방형 시스템 간 상호 연결(OSI) 모델의 네 번째 계층에서 작동합니다. 초당 수백만 개의 요청을 처리할 수 있습니다. 로드 밸런서가 연결 요청을 받으면 기본 규칙의 대상 그룹에서 대상을 선택합니다. 리스너 구성에 지정된 포트에서 선택한 대상에 대한 TCP 연결을 열려고 시도합니다
  - CLB(Classic Load Balancer)

### 기능
- EC2 인스턴스, 컨테이너, IP 주소와 같은 여러 대상에 대해 수신 어플리케이션 또는 네트워크 트래픽을 여러 가용 영역에 배포합니다. 

### ALB
- 이후 ECS 예제에서는  ALB 사용
- 구성은 로드밸런서, 리스너, 대상 그룹<br/>
![로드 밸런서 구성도](https://docs.aws.amazon.com/ko_kr/elasticloadbalancing/latest/application/images/component_architecture.png)
  - 로드 밸런서 : 클라이언트(ECS 클러스터의 서비스)에 대한 단일 접점 역할을 수행.  여러 가용 영역에서 EC2 인스턴스 같은 여러 대상(실행되고 있는 작업)에 수신 애플리케이션 트래픽을 분산
  - 리스너 : 프로토콜 및 포트를 사용하여 클라이언트의 연결 요청을 확인. 정의한 규칙에 따라 요청을 라우팅함.
  - 대상 그룹 : 프로토콜과 포트번호를 사용하여 등록된 대상으로 요청을 라우팅

## ECS구성 요소
- 클러스터 : 작업과 서비스의 논리적 그룹
- 클러스터 관리 엔진 : 클러스터 리소스 및 작업 상태 관리
- 작업 정의 (Task Definitions) : 작업에 대한 컨테이너 및 환경의 정의
- 작업 : 인스턴스에서 실제 실행되는 컨테이너 작업
- 스케쥴러 : 클러스터 상태를 고려한 작업 배치
- ECS 에이전트 : EC2 인스턴스 및 매니저와 통신

## 용어 정의
![](https://static.hubtee.com/files/7ff/7ff2d46916c9214e89a7cb5e40fe25d554c1f32d865eed7bb8b5efc076d03bf0.m.png)

### 클러스터와 컨테이너 인스턴스
![ECS cluster](https://static.hubtee.com/files/946/9468152073bec2df07ecfd01a1c05e025965ac18b76d523908415d9644dd3e5e.m.png)
- cluster
  - 도커 컨테이너를 실행할 수 있는 가상의 공간
  - 기본적으로 ECS 클러스터는 컴퓨팅 자원을 포함하지 않은 논리적 단위이다.
- 컨테이너 인스턴스
  - 클러스터에 연결된 EC2 인스턴스
  - EC2에 ecs-clinet 서비스를 실행하여 클러스터에 연결가능하고 연결된 인스턴스를 컨테이너 인스턴스라고 한다.
  - ec2-client는 컨테이너 인스턴스의 자원을 모니터링 및 관리하고 클러스터가 요청한 컨테이너를 실행

### 작업(task)와 작업정의(task definition)
![task and task definition](https://static.hubtee.com/files/325/32589e80f9b5f7c184b91fd16bc0dccd1f476e0cf8643eb8be009b4db1ba027a.m.png)
- 작업
  - ECS에서 컨테이너를 실행하는 최소 단위
  - 작업은 하나 이상의 컨테이너로 구성
  - 같은 작업으로 실행되는 컨테이너는 같은 컨테이너 인스턴스에서 실행되는 것이 보장됨
- 작업 정의
  - 태스크의 설계도 역할
  - 태스크를 실행하기 위한 설정(컨테이너 네트워크 모드, 태스크 role, 도커 이미지, 실행 명령어, 메모리,cpu 제한 등)을 정의한 리소스
  
- 작업은 클러스터에 종속적, 작업정의는 비종속적

### 서비스
![service](https://static.hubtee.com/files/c99/c9960bb6f78bbf08198503320e98e2aa93625f0ac1f4e11c382b384a5dfadb1a.m.png)
- 서비스는 하나의 task definition과 연결
- 레플리카 타입과 데몬 타입이 있음
  - 데몬 타입 : 모든 컨테이너 인스턴스에 행항하는 태스크가 하나씩 실행
  - 레플리카 타입 : 태스크의 수를 지정하여 태스크가 이 개수만큼 실행되도록 자동적으로 관리
- 서비스는 ecs-cliient에서 수집된 정보를 바탕으로 태스크 배치 스케줄링을 수행

## 실습
### 클러스터 생성하기
- 클러스터는 콘솔을 이용하여 생성
- EC2가 등록된 상태로 생성 가능. 이후 오토스케일링으로 인스턴스 수를 조정할 때 자동으로 등록되어 생성/삭제
- AWS ECS에 접속 > 클러스터 생성 클릭
![클러스터 생성](./img/cluster_create_01.png)
- 클러스터 탬플릿 > Linux + 네트워킹 선택 : 선택시 오토스케일링 그룹에 포함되어 있는 EC2 인스턴스 생성 가능
![클러스터 탬플릿 선택](img/cluster_template.PNG)
- 클러스터 이름 작성, 빈 클러스터 생성시 클러스터만 생성도 가능
![클러스터 이름](img/cluster_name.PNG)
- ECS에 등록될 EC2 인스턴스 설정
![](img/instance_configure.PNG)
  - 프로비저닝 모델 : 온디맨드 인스턴스와 스팟 인스턴스 선택 가능
  - 인스턴스 유형 : 실행할 컨테이너에 맞춰 인스턴스 타입 선택
  - 인스턴스 개수 : 생성할 인스턴스의 개수. 선택은 1개 이상으로 이후 오토스케일링을 통해 조정 가능. 
  - AMI ID : 기본 선택값이 ECS의 최적화된 AMI
  - EBS 스토리지 : 인스턴스의 스토리지 크기로 최소 22는 기본 설정
  - 키페어 : 이후 ssh로 접근하기 위한 키페어
- 컨테이너 인스턴스(EC2 인스턴스)에서 사용할 vpc 생성. 기존의 vpc도 사용 가능
![인스턴스 VPC 선택](img/container_instance_vpc.PNG)
  - VPC : 새 VPC 생성과 기존 VPC 사용선택 가능
    - 새 VPC 선택시 서브넷, CIDR 블록 작성
    - 기존 VPC는 이미 등록되어 있는 VPC 목록 중 선택
  - 보안그룹 : 보안 그룹 또한 새로 생성 혹은 기존 보안그룹 사용 가능
    - 새 보안그룹 인바운드 규칙을 작성
    - 기존 보안그룹은 기존의 VPC를 선택해야만 사용 가능. 그 이유는 보안 그룹은 VPC에 설정의 하나이기 때문. 기존의 보안 그룹 또한 
    - ![](img/select_sg.PNG)
- 역할은 따로 수정하지 않고 기본값으로 > 생성 버튼 클릭
- 생성 완료 후 ECS 클러스터에 등록된 클러스터 확인 가능
![생성된 클러스터](img/created_cluster.png)

### Task Definition 등록하기
- aws-cli를 이용하여 등록
- task-definition json 파일 작성

``` json
{# td-01.json
  "family": "ecs_flask_examaple",
  "networkMode": "bridge",
  "containerDefinitions": [
    {
      "name": "flask",
      "essential": true,
      "image": "<ECR에 uri>",
      "cpu": 0,
      "memory": 128,
      "portMappings": [
        {
          "hostPort": 5000,
          "containerPort": 5000,
          "protocol": "tcp"
        }
      ]
    }
  ]
}
```
- [파라미터 종류](https://docs.aws.amazon.com/ko_kr/AmazonECS/latest/developerguide/task_definition_parameters.html)
  - **family** : 작업 정의 등록할 때 지정. 패밀리는 개정 번호를 사용하여 지정된 작업정의의 여러 버전에 대한 이름. 작업 정의 이름으로 봐도 거의 무방함. \<family\>:\<개정번호\>로 사용할 작업을 지정
  - **networkMod** : 작업의 컨테이너에 사용할 도커 네트워킹 모드로 3가지 종류가 있음
    - bridge : 도커의 기본 가상 네트워크 사용
    - host : bridge 모드에서 제공하는 가상화된 네트워크 스택 대신 호스트 네트워크 스택을 사용하기 때문에 가장 높은 네트워킹 성능을 제공하지만, 노출된 컨테이너 포트가 해당 호스트 포트에 직접 매핑되므로 포트 매핑을 사용하는 경우 동적 호스트 포트 매핑을 활용할 수 없거나 단일 컨테이너 인스턴스에서 동일한 작업의 다중 인스턴스화를 실행할 수 없습니다.
    - awsvpc
  - **host port** : ec2 인스턴스에 접속할 포트(0으로 설정시 dynamic mapping 기능 사용 가능)
  - **containerPort** : 도커 컨테이너의 네트워크 포트
  
- 작업 정의 등록
  - `aws ecs register-task-definition --cli-input-json file://./td-01.json`

### Service 등록하기
- 생성후 ECS의 클러스터 > ECS 인스턴스 탭 > 컨테이너 인스턴스를 클릭하면 퍼블릭 DNS를 볼수 있다
- 퍼블릭 DNS:host-port 로 접속시 컨테이너에 올라간 페이지 확인 가능
- `aws ecs create-service --cluster=awesome-ecs-cluster --cli-input-json file://service-01.json`
```json
{# service-01.json
    "serviceName": "awesome-service",
    "taskDefinition": "ecs_flask_examaple:1",
    "desiredCount": 1
}
```

### 등록된 서비스에 로드 밸런서(ALB) 추가하기
![](https://static.hubtee.com/files/73f/73f92659e990a302e41b2c65bb14f417f4dd8b349bedfbddf36aba3624428772.m.png)
- 위에서 생성했던 서비스는 로드 밸런서
- 서비스에 로드 밸런서를 붙이기 위해서 로드 벨런서와 타겟 그룹이 필요
- 로드 밸런서 생성 : 생성에는 이름, 서브넷, 보안 그룹 필요
  - `aws elbv2 create-load-balancer --name new-ALB --subnets <subnetid1> <subnetid2> --security-groups <groupid> | jq .LoadBalancer[0] | .LoadBalancerArn `
    - name은 생성할 로드밸런서 이름
    - subnet은 적어도 2개 이상
    - security group은 80포트가 열러 있어야 함
  - `<LoadBalancer Arn>` : 위 명령어의 출력은 로드밸런서의 arn
  - 참고 : elbv2에서는 ALB와 NLB를 생성할 수 있음, type을 network로 지정하지 않으면 ALB가 생성됨
- 타겟 그룹 생성 : 생성시 이름, 프로토콜, port, VPC ID가 필요
  - ECS 서비스의 VPC와 동일한 VPC를 지정 
  - `aws elbv2 create-target-group --name <target-group-name> --protocol HTTP --port 5000 --vpc-id <vpc-id> | jq .TargetGroups[0] | .TargetGroupArn`
    - port는 서비스에서 생성하는 docker container의 port번호
    - vpc id는 연결할 ECS 서비스가 존재하는 vpc
  - `<Target Group Arn>` : 위 명령어의 출력은 타겟그룹의 arn
- 리스너 생성
  - 리스너는 로드 밸런서에서 트래픽을 받아 타겟 그룹으로 보내주는 역할
  - `cmd aws clbv2 create-listenner -- protocol HTTP --port 80 --load-balancer-arn <Load-Balancer-Arn> --default-actions 'Type=forward,TargetGroupArn=<TargetGroupArn>`
    - protocol, port : 로드밸런서가 직접 트래픽을 받는 프로토콜과 포트를 의미
    - default-actions : default로 전달해줄 타겟 그룹 지정

- 로드 밸런서를 생성한 후 서비스를 업데이트
  - 현재 존재하는 서비스를 중지
    - `aws ecs update-servioce --cluster Flask-test --service awesome-service --desired-count 0`
    - 

```json
{# service-02.json
  "serviceName": "awesome-service",
  "taskDefinition": "ecs_flask_examaple:1",
  "loadBalancers": [
    {
      "targetGroupArn": "arn:aws:elasticloadbalancing:ap-northeast-2:402595979923:targetgroup/awesome-tg/7c0408b1b8264a86",
      "containerName": "flask",
      "containerPort": 5000
    }
  ],
  "desiredCount": 1,
  "role": "arn:aws:iam::402595979923:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS"
}
```

### Dynamic Mapping 적용하기
- 위 서비스에서는 컨테이너의 포트가 정적으로 설정되어 있다. 따라서 하나의 컨테이너 인스턴스에서 2개의 작업이 불가능하다. 이를 보완하기 동적으로 포트를 설정해야 한다.
![Dunamic Mapping이 적용된 ECS 구성도](https://static.hubtee.com/files/2c9/2c906601851ad377176f518c74d2f711c5b21a4747aa74ba75d1d3fcee555a0a.m.png)
- 동적 맵핑을 적용하기 위해서는 작업

### ECS Auto Scaliling

- 용량 공급자 (Capacity Provider)
  - ECS에서 작업에 사용할 인프라를 설정
  - 작업이나 서비스가 실행될 때 인스턴스가 없을 때는 새로 생성하여 시작 가능
  - Fargate에는 `Fargate`, `FARGATE_SPOT` 용량 공급자가 자동으로 제공
  - EC2 인스턴스는 Auto Scailng Group을 통해 용량 공급자를 설정한다.
- 용량 공급자 전략
  - 용량 공급자 전략을 사용하면 작업에서 하나 이상의 용량 공급자를 사용하는 방법을 제어 가능
  - 작업을 실행하거나 서비스를 생성할 때 용량 공급자 전략을 지정.
  - 용량 공급자 전략은 각 공급자에 대해 선택적 기본 및 가중치가 지정된 하나 이상의 용량 공급자로 구성.
- 기본 용량 공급자 전략
  - 기본 용량 공급자 전략은 ECS 클러스터를 생성할 때 연결
  - 작업을 실행하거나 서비스를 만들 때 다른 용량 공급자를 지정하지 않는 경우에 사용할 전략으로 지정
- 고려 사항
  - 전략을 지정할 때 지정할 수 있는 용량 공급자 수는 6개로 제한
  - 클러스터에 ASG(EC2 인스턴스)와 Fargate 용량 공급자가 포함될 수 있지만 용량 공급자 전략은 둘 중 하나만 포함될 수 있다.
  - 실행되고 있는 서비스가 있고 그 서비스에 Scale-out이 필요할 때 유용
  - CloudWatch의 경보를 이용한 인스턴스 Scaling. CPU, 메모리 점유율 등을 사용하여 Scaling
  - 용량 공급자는 한 번 생성되고 나면 수정, 삭제가 불가 [이슈](https://github.com/aws/containers-roadmap/issues/632)
    - 사용시 문제점 확인 필요
  


# 참고
- [AWS ECS 구성하기 Part1](https://waspro.tistory.com/426?category=855358) : part 1~3으로 나누어져 있음
- [ECS 사전지식](https://yongho1037.tistory.com/732)
- [aws-cli로 ecr 서비스 배포하기](https://www.44bits.io/ko/post/container-orchestration-101-with-docker-and-aws-elastic-container-service)
- [AWS CLI로 ALB 생성](https://docs.aws.amazon.com/ko_kr/elasticloadbalancing/latest/application/tutorial-application-load-balancer-cli.html)
- [ECS/Dynamic Port Mapping](https://www.youtube.com/watch?v=lCWy1sXU79E)
-[ECS Auto Scaling] (https://docs.aws.amazon.com/ko_kr/AmazonECS/latest/developerguide/cluster-auto-scaling.html)
- [ECS 클러스터 용량 공급자](https://docs.aws.amazon.com/ko_kr/AmazonECS/latest/developerguide/cluster-capacity-providers.html)
-[용량 공급자 수정/삭제 불가 이슈](https://github.com/aws/containers-roadmap/issues/632)