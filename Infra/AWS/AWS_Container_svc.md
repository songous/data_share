# AWS ECS (Elastic Container Service)
## 특징
- 모든 규모의 클러스터를 쉽게 관리
- 유연한 컨테이너 배치
- 다른 AWS 서비스와 연동하도록 설계
- 확장성, 속도 뛰어남
- 보안
- ec2 container service -> elastic container service : fargate의 등장과 함께 변화한 걸로 추측
- 리소스 관리를 직접해야 함

## 장점
- ECS는 클러스터 관리가 필요 없음

## 요금
- 사용시 별도의 요금 없음
- EC2, fargate의 요금만 부과

## 기존
- EC2 인스턴스 생성
- EC2 인스턴스에 docker 설치
- docker swarm / kubernetes를 통해 container 배포

## gitlab CI/CD 연동
- [AWS ECS GitLab CI/CD 시나리오](https://sarc.io/index.php/aws/1748-aws-ecs-gitlab-ci-cd)


# AWS EKS (Elastic Kubernetes Service)
## 특징
- kubernetes upstream service로 다른 kubernetes의 설정파일들을 모두 사용할 수 있다.
- 워커 노드 당 Pod 개수 제한이 있음.(http://engineering.vcnc.co.kr/2019/02/eks-migration/)
  - of ENI * (# of IPv4 per ENI - 1)  + 2


## 단점
- 마스터 노드만 관리, 워커 노드는 따로 관리를 해야함
 - 워커노드는 node group으로 관리 또는 fargate를 사용해야 함
-

# Fargate
- 관리할 클러스터 없음
- AWS Fargate에서는 컨텡너만 생각
- app 구축 및 운영에만 집중
- EC2 인스턴스 필요 x


# 참고
- [How AWS Fargate Turned Amazon EKS into Serverless Container Platform](https://thenewstack.io/how-aws-fargate-turned-amazon-eks-into-serverless-container-platform/)
- [AWS EKS를 사용해 K8S 클러스터 생성하기](https://alicek106.tistory.com/24)
- 