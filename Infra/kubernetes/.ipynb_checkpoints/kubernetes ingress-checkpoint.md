# Kubernetes Ingress

## Ingress란?
- 인그레스는 클러스터 외부에서 내부로 접근하는 요청들을 어떻게 처리할지 저어의해둔 규칙들의 모음
- 인그레스

## 설치
- `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/mandatory.yaml`

## 참고 
- [AWS kubernetes ingress controller part 1](https://itnext.io/kubernetes-ingress-controllers-how-to-choose-the-right-one-part-1-41d3554978d2)
- [인그레스 컨트롤러](https://arisu1000.tistory.com/27840)
- [ingress-nginx 설치](https://kubernetes.github.io/ingress-nginx/deploy/)