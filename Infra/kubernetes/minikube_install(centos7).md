# CentOS 7에서 minikube 설치하기
## 설치
- 참고 [centos에 minikube 설치시 생기는 issue](https://gist.github.com/Expire0/7c04f75871e9eac5019a97ea6eed20cc)
- [minikube-installer.sh](https://github.com/Expire0/Docker_files/blob/master/kubernetes/minikube-installer.sh)
- 위 minikube-installer.sh의 내용을 복사하여 붙여넣기
- chmod +x minikube-installer.sh
- sudo ./minikube-installer.sh
- `sudo minikube status`를 통해 running 상태인지 확인