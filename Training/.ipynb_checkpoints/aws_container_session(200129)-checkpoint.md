- 발표자 : 정영준 솔루션즈 아키텍트
- 컨테이너 서비스를 활용한 오케스트레이션 실습


## 컨테이너 서비스 오버뷰
- 컨테이너 런타임 인터페이스
- fargate : 서버리스 컨테이너 서비스
  - docker 이미지만 있으면 서비스 가능
  - k8s도 운영 가능
- ecr : 단순 이미지 저장에서 이후 관리, 사이닝, 보안, 모니터링 추가

- k8s가 80, ecs가 2위(참고 cncf servey), ecs의 사용량 증가 추세
- fargate는 fire cracker로 작동. docker 아님
  - ecs가 대체적으로 저렴
  - ec2 기반 ecs, fargate
- AWS Cloud Map : advanced DNS
- APP Mesh : istio와 비슷. service mesh

## EKS
- upstream k8s를 managing
  - 실제 k8s와 동일한 코드. 다른 k8s 클러스터에서 사용한 스크립트 재사용 가능
- k8s의 사용이유
  - 기존 오토메이션 툴과는 다름. 명령형 인프라가 아닌 선언형 인프라
  - 기본적으로 설정한 리소스 등을 유지하기 위해 k8s가 직접 관리. IsC를 k8s로 구현
  - 실제 devops를 운영하기 위해서는 광범위한 지식이 필요함
- 마스터 노드를 운영하면서 scale up, down을 해야함. eks는 이 작업을 대신 해줌

- CDK(cloud development kit)
- managed worker node : ec2를 관리
  - managed kubernetes data plane phase 2
  - addon? phase 3가 올해 오픈 예정
- eks private endpoint
- autoscaling 

- multi staging build
- codepipeline 

- EKS architecture
  - NLB를 통해 외부에서 접근
  - ssl/tls 인증을 통해서
  - 최소 6개의 c type 인스턴스를 이용해서 서비스. 1개의 eks 클러스터 당
  -  2시간 마다 snap shot을 떠서 복구 가능
- IAM Authoentication : k8s의 메인 컴포넌트로 포함되어 있음
- kubernetes data plane

## hand-on
- https://eksworkshop.com/
- [cloud9 환경 생성](https://eksworkshop.com/020_prerequisites/workspace/)
  - t1, t2 환경으로 생성해도 상관없지만 build할 때 리소스 부족으로 안 될 수 있음

- managed worker node 

## EKS debugging
- break point를 확인하는 것과는 다름. 각 오류마다 다른 노드에 떨어짐
- logging에서 원하는 log를 cloudwatch에서 볼 수 있음
- service 설정 확인
- test pod에서 통신 테스트. pod는 기본적으로 외부와 통신할 수 있는 endpoint가 없음
  - service 설정 확인
  - expose 해야함
  - DNS가 정상적으로 동작하는지 확인. 
- IP 설정 확인
  - pod에서 ip설정 확인 가능
  - crul ip 
- service configration 확인
  - kubectl get svc hostnames -o json
  - targetport, nodeport 확인, 정수,문자열 타입 확인
- service endpoint 확인
  - kubeproxy보다는 ALB, NLB를 추천(aws 서비스의 경우)
- pod 상태 확인
  - kubectl describe pods <pod_name>
  - pod 로그 확보가 안 될 때는 워커 노드의 리소스 양을 확인
  - host log를 확인
  - kubectl logs --previous <pod_name> <container_name>

## EKS monitoring
- 여러 plug-in을 설치해야함(prometheus, grapana)
  - helm을 이용하여 설치
    - helm-Tiler
    - helm 3.0에서 tiler가 k8s의 기본적으로 들어감
    - chart : helm을 이용해 배포를 할 때 기본적인 설정. templet component
    - 
- log
  - fluentd와 elasticsearch를 이용해 로그를 수집하는 것이 가장 좋다.
  - fluentd 사용시 주의점
    - fluentd.config 설정
    - cpu, memory 리소스 설정. 로그의 양에 따라 설정을 바꿔야함
    - 로그 10만 line 당 400mb 사용
    - fluentbit은 fluentd보다 메모리가 5배 정도 차이

- ELK가 복잡할 땐 cloudwatch를 사용
 - cloudwatch에서 container insight 사용
  
  
- 디버깅, 로깅, 모니터링
- ecs,eks
  - 차이점 : 컨트롤 plain, ecs는 아마존에서 직접 제공, 컨테이너를 올리기 위해 사용,
  - eks는 k8s를 제공 그리고 다양한 third party 툴을 제공

# 질문
- EKS 과금은 클러스터에 부과?

