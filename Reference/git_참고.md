# git 참고 사항

## git error 관련
- Permission denied (publickey) : gitlab에 ssh 키 추가하여 해결
    - ssh-keygen id_rsa.pub
      - 공개키 생성시 비밀번호를 입력하면 안됨
    - id_rsa(비밀키), id_rsa.pub(공개키)
    - `cat ~/.ssh/id_rsa.pub`으로 나온 키값을 복사
    - gitlab의 우측상단의user의 setting으로 이동
    - 좌측에 ssh keys에서 복사한 키값을 입력하면 이후 작업 가능