# Human Pose Estimation
----------------------------
1. 설명
    - Pose Estimation 컴퓨터 비전에서 객체의 위치(Position)과 방향(Orientation)을 탐지하는 문제
    - Human Pose Estimation은 사람의 관절인 keypoint(parts)가 어떻게 구성되어 있는지 위치를 츶겅(Localiztion)하고 추정(Estimation)하는 문제
    - 성능 평가 : Percentage of Correct Keypoint(PCK) 지표사용
        - PCKh : using head size instead of bounding box
    - 2D Pose Estimation, 3D Pose Estimation으로 분류
    - 추정 방식은 Top-down, Bottom-up 
        - top-down 
            - 먼저 사진에서 인물을 detecting하고 그 다음 part와 pair을 찾음
            - 정확도는 bottom-up 방식에 비해 높은
            - multi-person일 경우에 Detecion된 사람마다 pose를 추정하기 때문에 속도가 느림
        - bottom-up
            - 이미지 내의 모든 사람의 part를 찾은 후 part를 grouping/associating한다.
            - Detection 과정을 거치지 않아 속도가 빨라 Real-time에 적용하기 적합 

2. Dataset 종류
    - MPII Human Pose : 25K image containing over 40K people, 410 human activity
    - Leeds Sport Poses
    - FLIC Wrists
    - FLIC Elbows
    - COCO
    
# Open Pose
------------------------------
## 사용 개념
- Part Affinity Fields : 추출된 관절 구조가 어떤 객체의 것인지 분류하는데 사용
- Part Conffidence Maps : 인간의 관절 구조 등을 찾는데 사용

![Steps involved in human pose estimation using openpose](https://miro.medium.com/max/1211/1*WClpOwPiG4Glg6WOWlNK_Q.png)

- body, foot, hand, facial keypoints를 찾기 위한 2D pose Detector
  
![](http://openresearch.ai/uploads/default/original/1X/f337f75048543923f41beb7cc9430d1e8c4c2eda.png)
- 동일한 구조를 반복적으로 연결하여 refine
- 각각의 branch에서 confidence map과 PAF를 predict

- opencv를 통해 사용 가능

## 참고
- [OpenPose 깃허브](https://github.com/CMU-Perceptual-Computing-Lab/openpose)
- [OpenPose 설명 블로그](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/.github/Logo_main_black.png)
- [OpenPose 논문](https://arxiv.org/abs/1611.08050)
- [OpenPose Colab 예제 ](https://colab.research.google.com/drive/12ej3tmpp1Iz64h58gVzU53mxGfIoSvQO#scrollTo=FOdkDhb6ga6N)
- [Human Pose Estimation 최신 연구 동향](https://eehoeskrap.tistory.com/329)
- [multi-person pose estimation in opencv using openpose](https://www.learnopencv.com/multi-person-pose-estimation-in-opencv-using-openpose/)
- [OpenPose in OpenCV 예제](https://github.com/opencv/opencv/blob/master/samples/dnn/openpose.py)
- [Deep Learning Based 2D Human Pose Estimation A Survey](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=8727761) : 2d hyman pose estimation에 대해 자세히 설명

# DensePose
---------------------------------
- mask R-CNN 기반
- 깊이에 따른 표면을 출력
- COCO 데이터에 dense