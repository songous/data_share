# Deep Learning Based 2D Human Pose Estimation: A Survey
### Qi Dang, Jianqin Yin, Bin Wang, and Wenqing Zheng
--------------------------
### 1. 서론
- human pose estimation에 대한 설명
    - 사용 분야, 목적, single-person/multi-person algorithm
- human pose estimation 문제를 해결하기 위한 방법
    - 과거에는 hand-crafted feature를 이용(hog, Edgelet?)
    - 현재는 deep learing 기반의 방법들로 해결. 이전 hand-crafted 방식 보다 뛰어남(outperformed non deep SOTA methods with a big margin)
- 이전 연구에서 human pose estimation을 위한 deep learing에 대한 연구들을 검토하지 않음
- 이 논문의 목적은 human pose estimation을 위한 sota deep learning 방법의 종합적인 개요와 연구 트렌드를 보여주는 것

![taxonomy of this review](img/taxonomy_of_PoseEstimation.PNG)
- 이 논문에서는 위 방식으로 human estimation을 분류
     - 2D Pose Estimaition은 single/multi pose estimation으로 분류됨. 그 이유는 서로 동작하는 방법이 다름
     - single pose estimation은 이미지 내에 있는 한 사람의 pose를 찾음
         1. 바로 keypoint 회귀분석을 위해 feature map 결과를 이용한다.
         2. heatmap을 생성한 이후 그것을 기반으로 예측한다.
     - multi pose estimationd은 이미지 내의 다수의 사람의 pose를 찾음
         1. top-down : human detection과  single pseron keypoint estimation으로 구성
         2. bottom-up : 이미지 내의 모든 키포인트를 찾고 사람별로 키포인트를 묶는다.
- 이전에도 pose estimation에 대한 survey들이 있었지만 대부분 hand- crafted feature 기반이고 deep learning에 중점을 두고 있지 않음

### 2. Single-Person Pipeline
![single-person-pipeline](img/single-person-pipeline.PNG)
- 이 논문에서는 a: Direct regression based framework, b: Heatmap based framework 라고 명명

#### 2.1 Direct regression based framework 관련 논문
- A. Toshev and C. Szegedy, Deeppose: Human pose estimation via deep neural networks 
    - cascadeed DNN regressor 제안
    - 다른 procedures가 없이는 feature map으로 부터 mapping하기 어려움
    - 논문에서 제안한 방법은 top-down 방식의 multi-person pose estimation임 
- Carreira et al., Human pose estimation with iterative error feedback
    - 자가 수정 모델을 사용. 예측한 위치를 점진적으로 개선해 나감.
- Sun et al., Compositional human pose regression
    - structure-aware approach을 제안. 관절 대신 뼈를 pose representation 파라미터로 사용. 
    - 원시적, 안정적, 학습이 쉬움
- Luvizon et al., Human pose regression by combining indirect part detection and contextual information
    - heatmap을 좌표로 바꾸기위한 soft-max를 제안
    
#### 2.2 Heatmap-based framework 관련 논문
-  Chen and Yuille,Articulated pose estimation by a graphical model with image dependent pairwise relations
    - pariwise realtion에 대한 __graphical model__을 사용
- Chen et al., Adversarial posenet: A structure-aware convolutional network for human pose estimation
- __Convolutional Pose Machines(CPM)__ : 여러 단계에서 heatmap을 회귀분석한다.
- Newell et al. __stacked hourglass__라고 불리는 network 구조를 설계
    - bottom-up, top-down 프로세스을 반복해 성능 증가
-  Chu et al., Multi-context attention for human pose estimation
    - stacked hourglass에 기조한 baseline model을 만듬
    - multi-context attention mechanism을 이용. 모델을 더 탄탄하고 정확하게 만들기 위해 사용
    -기존 stacked hourglass에 hourglass residual unit을 추가
- Martinez et al., A simple yet effective baseline for 3d human pose estimation
    - 3D keypoint를 예측하기 위해 2D keypoint를 사용하는 것을 제안
    - 2D detection은 3D human pose estimation error의 주요 원인 중 하나임
    
#### 2.3 어떤 framework가 더 뛰어난가, direct regression or heatmap based?
- direct regression
    - 장점 : quick and direct, end-to-end 방식으로 학습된다
    - 단점 : learning mapping이 어렵다, multi-person case에 적용할 수 없다.
- Heatmap-based
    - 장점
        - 시각화가 쉽다.
        - 복잡한 케이스에 적용할 수 있다.
    - 단점
        - 고해상도의 hea map을 위해 많은 메모리가 소비된다.
        - 3D 시나리오로 확장하기 어렵다.

### 3. Multi-Person Pipeline
- keypoint detection과 hyman location이 핵심 문제이다.
- 위 문제 해결을 위해 Top-down pipeline, Bottom-up pipeline이 제안되었다.

#### 3.1 Top-down pipeline
![top down pipeline](img/top-down-pipeline.PNG)

- 기본적인 pipeline
   1. 이미지 내의 모든 사람에 대한 BBox를 찾음
   2. 각 사람에 대해 single-person approches를 사용
   
   
- Toshev and Szegedy, top down approch인 DeepPose 제안
    - the face based body detector로 사람의 위치를 대략적으로 추정하고 multi-stage cascade DNN 기반의 joint coordinate regressor를 이용해 관절 좌표를 직접 regression한다.
   
   
- Radosavovic et al., Data distillation: Towards omni-supervised learning
    - semi supervied learing을 이용한 data augmentation 방법
    - sota human pose detector가 self-training 기술에 적용되기 충분한 정확도를 가지고 있다고 증명함
    
    
- Fang et al.RMPE: Regional multi-person pose estimation
    - Human detection와 human box alignment에 대해 연구
    - single-person pose estimation은 human detector의 성능이 중요하다. human detector의 성능이 낮다면 single-person pose estimation의 성능에 큰 영향을 미친다.
    -   Symmetric Spatial Transformer Network(SSTN)+parallel Single-Person Pose Estimator(SPPE)으로 높은 수준의 single-person region을 추출
    - Mask R-CNN이 동시에 human bbox와 keypoint를 예측
    
    
- U. Iqbal and J. Gall, Multi-person pose estimation with local joint-to-person associations
    - keypoint estimation within the human detection box
    - 공동 대 개인 연결 문제 : occlusion 문제를 해결하는 것이 목표
    - 동일한 box 내에 있는 중복된 사람의부분을 지역적으로 keypoint를 연결하여 제거 한다. Deepcut을 기반으로 작동
    
    
- Papandreou et al., Towards accurate multiperson pose estimation in the wild
    - Faster R-CNN과 유사한 아키텍처
    - joint dense heat map과 position offset을 동시에 예측
    - 그 이후 2가지 결과를 종합하여 keypoint 위치를 얻음
    - 관절 중심으로 일정 부위에서 keypoint를 향하는 백터를 학습시켜 이후에 voting을 통해 관절의 위치를 예측한다.
    
    
- Chen et al, Cascaded pyramid network for multi-person pose estimation
    - Cascaded pyramid network라는 구조를 제안
    - GlobalNet, RefineNet으로 구성되어 있다.
    - GlobalNet은 쉬운 feature representaion을 잡을 수 있고 RefineNet은 어려운 keypoint만 다루는데 사용된다.
    
    
- 후처리 방법
    - A data-driven pose NMS(Non-Maximum Suppression) : RMPE에서  occlussion를 해결하기 위해 제안

#### 3.2 Discussion for top-down approaches
##### 3.2.1 top-down human pose estimation에서 human detector가 중요한가?
- 정확도, 메모리, 시간의 trade-off를 고려해야 함
- human detector의 성능이 좋을 수록 human pose estimator의 정확도가 증가
- human detector의 AP가 증가하는 것 보다 human pose estimator의 AP가 증가하는 것이 더 느리다. 
- 일정 수준 이상이 되면 estimator의 정확도는 더 이상 증가하지 않음. detector가 모든 인물을 찾더라도 estimator가 정확하지 않으면 정확도는 증가하지 못하기 때문
![estimator/keypoint mAP graph](img/human-pose-estimator-AP.PNG)

##### 3.2.2 NMS
- 중복 검출을 중에서 더 정확한 결과를 거르기 위한 작업
- human detection에서 사용한 방법 : standard NMS, soft-NMS
    - soft-NMS : 기존 NMS와 다르게 IOU가 겹칠 때 0으로 바꾸는 것이 아니라 score를 내린다.
- Part-based NMS : 군집을 무게중심으로 대체함으로서 part를 합친다
- parametric pose NMS : 모든 파라미터가 데이터로 학습 됨. Part-based NMS보다 빠르지만 soft-NMS보다 복잡하다.

### 3.3 Bottom-up multi-person pipeline
![bottom up pipeline](img/bottom-up-pipeline.PNG)
- 기본적인 pipeline
    1. 이미지 내의 모든 keypoint를 찾는다
    2. 각 사람별로 keypoint를 grouping
    3. grouping한 결과를 선으로 잇는다

## 참고
- [Graphical Model이란 무엇인가](https://medium.com/@chullino/graphical-model%EC%9D%B4%EB%9E%80-%EB%AC%B4%EC%97%87%EC%9D%B8%EA%B0%80%EC%9A%94-2d34980e6d1f)
- [baseline model이란?](https://machinelearningmastery.com/how-to-know-if-your-machine-learning-model-has-good-performance/)
- [end to end ML](https://www.edwith.org/deeplearningai3/lecture/34893/)
-[Data Distillation 요약](https://ko-kr.facebook.com/groups/TensorFlowKR/permalink/572627283078334/)
- [CPM](http://openresearch.ai/t/cpm-convolutional-pose-machines/76)