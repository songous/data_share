# TODO List

1. gitlab CI/CD (60%)
  - ~~gitlab runner의 shell executor 동작 확인~~
    - ~~window 환경에서 runner 설치 및 CI/CD 확인~~
    - ~~linux 환경에서 runner 설치 및 CI/CD 확인~~
  - gitlab runner, docker 환경에서 사용법 확인
    - ~~docker에서 사용하는 gitlab runner 동작 방법 확인~~
    - ~~운영체제(windows, linux)에서의 config 차이 확인~~
    - ~~windows에서 docker위의 runner를 이용한 배포~~
      - linux에서는 동작하지만 window에서는 동작하지 않음, 추후 필요시 확인
  - gitlab ci를 이용한 CI/CD 방법 확인(.gitlab-ci.yml)
    - .gitlab-ci.yml 작성 방법 확인
    - 운영체제, executor 별 CI/CD 차이점 검토
  - ~~gitlab ci와 k8s 연동(진행중)~~
    - k8s executor
  - docker image 관리 정책 
  - gitlab-ci와 ECS/EKS/ECR 연동 방법
    - ECS/EKS 기능 및 gitlab-ci와의 연동 확인
    - ECR 기능 확인


2. jupyter hub (20%)
  - **Jupyterhub의 아키텍쳐 확인**
  - **실제 Jupyterhub 사용할 때의 아키텍쳐**
  - 기능 확인
    - 계정 관리
    - 사용자별 개발환경 설정
    - extension 확인(git)
  - jupyterhub 배포 실습


3. gitlab 기능 확인하기
  - gitlab에서 issues 관리하는 방법 확인하기 
  - group 
  - gitlab image registry
  - wiki
  - snippets
  - settings


# Doing

1. gitlab ci와 ECS 연동 확인


2. CI/CD 과정 중 기존에 있는 이미지와 실행중인 컨테이너 처리
  - docker images 버전, docker image registry 관리 문제
    - 이미지 이미 존재하면 지우고 다시 build?
  - 실행 중인 컨테이너 관리. 지우고 다시 run할 때 컨테이너 중지는 막을 수 없음
    - 이건 나중에 docker compose 등 docker orchestration 툴로 해결 가능  




## 현재 진행률
- gitlab CI/CD(60%) 
- jupyterhub 개발 환경 배포(20%)
  