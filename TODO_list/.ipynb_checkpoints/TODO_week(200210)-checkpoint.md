### 금주 예정 사항(2020. 2. 10. ~ 14.)
- 2.10. 
  - ~~gitlab-ci와 k8s 구성 아키텍쳐 검토~~
  - ~~gitlab-ci와 k8s 참고 자료 검토 및 정리(컨퍼런스, Docs 등)~~

- 2.11. 
  - ~~gitlab에 k8s cluster를 추가 ~~
  - ~~gitlab에  추가된 k8s cluster를 이용한 CI/CD 확인~~
  - ~~gitlab에 k8s cluster를 추가했을 때, 사용 가능한 기능 확인~~
  - ~~위 환경에서 k8s executor 사용 방법 확인~~
  
- 2.12. 
  - ~~gitlab runner를 k8s 클러스터에 설치~~
  - ~~k8s에 설치된 runner를 이용한 CI/CD 확인~~
  - ~~위 환경에서 k8s executor 사용 방법 확인~~
  
- 2.13. 
  - ~~jupyterhub 기본 아키텍쳐 조사 및 검토~~
  - docker, k8s 등 사용 환경별 아키텍쳐 조사 및 검토
    - 각 환경별 설치 방법, 동작 예제 검토 등
  
- 2.14. 
  - 검토된 jupyterhub 예제에 사용자별 환경 설정 추가
  - extension(git 연동 등) 추가 검토
  
### 결과
- gitlab ci를 통한 k8s cluster에 배포 완료
- jupyterhub 미완료