### 전주 진행 사항(2020. 3. 30. ~ 4. 3.)

- 3.30 (월)
  - Kubernetes 환경에서 구축했던 서비스 Openshift로 이전
  - Openshift 구조 및 사용법 조사
  - Openshift 환경에 Jupyter Enterprise Gateway 설치
- 3.31. (화)
  - Openshift 내 root 권한 사용으로 인한 Permission 이슈 해결
  - Enterprise Gateway에서 호스트 Docker 접근 권한으로 인한 connection 이슈 해결
- 4.1. (수)
  - Enterprise Gateway에서 호스트 Docker 접근 권한으로 인한 connection 이슈 해결
  - Openshift 환경에 ldap 설치
  - Openshift JupyterHub 설치
- 4.2. (목)
  - JupyterHub 내 노트북 생성시 Persistent Volume 생성 이슈 해결
  - JupyterHub와 ldap 연동
  - JupyterHub와 Jupyter Enterprise Gateway 연동
- 4.3. (금)
  -  Enterprise Gateway에서 kernel 생성시 namespace 생성 불가 이슈 해결
  
### 금주 예정 사항 (2020. 4. 6. ~ 10.)

- 4.6. ~ 7. (월~화)
  - Jupyter Enterprise Gateway  namespace 생성 불가 이슈 해결
  - JupyterHub에서 노트북 생성 시 PV 자동 생성 불가 이슈 해결
  - JupyterHub와 EG간 정상 작동 확인
- 4.7. ~ 10 (수~금)
  - ldap의 config 및 데이터 보관을 위한 Persistent Volume 연결
  - Jupyter Enterprise Gateway kernel과 notebook 간의 volume 공유
  - Openshift 환경 내 JupyterHub 설치 및 사용방법 정리