### 전주 진행 사항(2020. 4. 6. ~ 10.)

- 4.6 (월)
  - helm을 통한 enterprise gateway 설치
    - namespace 생성 불가 이슈 해결
  - JupyterHub에서 노트북 생성 시 PV 자동 생성 이슈 검토
  
- 4.7. (화)
  - openshift 서비스를 okd로 이관
  - openshift에서 제공하는 로컬 provisioner 테스트
  - hostpath-provisioner 검토
  
- 4.8. (수)
  - hostpath-provisioner를 통한 PV 생성문제 해결
  - nfs-provisioner 서비스 추가를 통해 hostpath에서 발생한 서로 다른 노드에 존재하는 pv와 pod 간의 연결 문제 해결
  
- 4.9. ~ 10 (목~금)
  - ldap에 설정한 계정 유지를 위한 PV 추가
    - helm을 이용하여 설치
    - nfs storageclass 사용
  - jupyterhub 설치법 작성
  
### 금주 진행 및 예정 사항 (2020. 4. 16. ~ 17.)


- 4.13. ~ 14. (월~화)
  - okd 환경에 gitlab runner 설치
  - gitlab-ci 파이프라인 검토
    - access token을 이용한 private repogitory 접근
    - docker in docker를 이용한 image build
  - gitlab runner에서 생성되는 작업 pod에 airflow pv 연결
    - pv 및 pvc 작성
    - helm chart의 configmap.yaml 수정
    
- 4.16. ~ 17 (목~금)
  - jupyterhub와 jupyter enterprise gateway 사이의 volume 공유
  